jQuery(document).ready(function(){
	jQuery('.term_type_li .item').click(function(){
		ToggleItem(jQuery(this));
	})

	function ToggleItem(item){
		item.parent().find('.children').toggle();
		item.toggleClass('closed');
		if(item.hasClass('closed'))
			item.find('span > span').text('+');
		else
			item.find('span > span').text('-');
	}

	jQuery('.term_type_li .item').each(function(){
		jQuery(this).find('span').prepend('<span style="float: left; padding: 0 1rem;">-</span>');
	})
})