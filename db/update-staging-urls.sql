UPDATE `wp_blogs` SET `domain` = 'globaltroxler.wjstage.net' WHERE `blog_id` IN (1,2);
UPDATE `wp_blogs` SET `path` = '/' WHERE `blog_id` IN (1);
UPDATE `wp_blogs` SET `path` = '/training/' WHERE `blog_id` IN (2);

UPDATE `wp_options` SET `option_value` = 'http://globaltroxler.wjstage.net' WHERE `option_name` = 'siteurl';
UPDATE `wp_options` SET `option_value` = 'http://globaltroxler.wjstage.net' WHERE `option_name` = 'home';

UPDATE `wp_site` SET `domain` = 'globaltroxler.wjstage.net', `path` = '/' WHERE `id` IN (1);

UPDATE `wp_sitemeta` SET `meta_value` = 'http://globaltroxler.wjstage.net' WHERE `meta_key` = 'siteurl' AND `site_id` = 1;

UPDATE `wp_2_options` SET `option_value` = 'http://globaltroxler.wjstage.net/training' WHERE `option_name` = 'siteurl';
UPDATE `wp_2_options` SET `option_value` = 'http://globaltroxler.wjstage.net/training' WHERE `option_name` = 'home';