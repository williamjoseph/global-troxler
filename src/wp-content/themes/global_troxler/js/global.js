$(document).ready(function(){
	'use strict';
	
	$(document).foundation();

	var slideout = new Slideout({
	    'panel': document.getElementById('wrapper'),
	    'menu': document.getElementById('slideout'),
	    'padding': 285,
	    'tolerance': 70,
	    'side': 'right',
	    'touch': false
	  });

	$('.mobile-toggle').click(function(){
		slideout.toggle();
		return false;
	});

	$('.mobile-nav > ul > li > a').click(function(){
		var currentMenu = $(this).parent().find('.sub-menu');
		$(this).closest('.mobile-nav').find('.sub-menu').not(currentMenu).slideUp();
		currentMenu.slideToggle();
		return false;
	});
// console.log($('form.variations_form.cart'));
	$(document).on('found_variation', 'form.cart', function(event, variation){
		SetBackgroundImage(variation);
	});

	$(document).on('reset_image', 'form.cart', function(event, variation){
		SetBackgroundImage(variation);
	});

	function SetBackgroundImage(variation){
		var image = $('.images .product-image');

		if ( variation && variation.image_src && variation.image_src.length > 1 ) {
			image.css("background-image", "url('"+variation.image_src+"')");
		} else {
			var originalImageSrc = image.closest('[data-o_href]').attr('data-o_href');
			if(originalImageSrc !== undefined)
				image.css("background-image", "url('"+originalImageSrc+"')");
		}
	}
});