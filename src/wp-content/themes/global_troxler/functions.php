<?php

add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;

// Register Menus
register_nav_menus(
	array(
		'main-menu' => __( 'Main Menu', 'globaltroxler' ),
		'top-menu' => __( 'Top Menu', 'globaltroxler' ),
		'top-footer-menu' => __( 'Top Footer Menu', 'globaltroxler' ),
		'bottom-footer-menu' => __( 'Bottom Footer Menu', 'globaltroxler' )
	)
);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}

//Gforms hide labels

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// filter the Gravity Forms button type
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
	if($form['id'] == 1)
    	return "<button class='button submit-button small' id='gform_submit_button_{$form['id']}'>Submit</button>";
    return $button;
}

//Add Stylesheets

function stylesheets() {
 wp_enqueue_style('foundation', get_stylesheet_directory_uri() . '/css/app.css', __FILE__);
 wp_enqueue_style('fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', __FILE__);
 wp_enqueue_style('opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic', __FILE__);
 wp_enqueue_style('opensanscondensed', 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700', __FILE__);
 wp_enqueue_style('slideout', get_stylesheet_directory_uri() . '/css/slideout.css', __FILE__);
}

//Add Javascripts

function javascripts() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js');
 wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js');
 wp_enqueue_script('global', get_stylesheet_directory_uri() . '/js/global.js');
 wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js');
 wp_enqueue_script('foundation', get_stylesheet_directory_uri() . '/js/foundation.min.js');
 wp_enqueue_script('slideout', get_stylesheet_directory_uri() . '/js/slideout.min.js');
}

add_action ('wp_enqueue_scripts', 'stylesheets', 10);
add_action ('wp_enqueue_scripts', 'javascripts', 1);

require_once(dirname(__FILE__) . '/lib/ThemeSettings.php');

add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}



if(function_exists("WC")){
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
	remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 25 );
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_email_customer_details', array( WC()->mailer(), 'email_addresses' ), 20, 3 );
}
// print_r(WC()->mailer());
add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 100 );

function global_troxler_get_product_image_url( $product, $size = 'shop_thumbnail', $attr = array(), $placeholder = true ) {

	if ( $product->variation_id && has_post_thumbnail( $product->variation_id ) ) {
		$image = get_the_post_thumbnail_url( $product->variation_id, $size, $attr );
	} elseif ( has_post_thumbnail( $product->id ) ) {
		$image = get_the_post_thumbnail_url( $product->id, $size, $attr );
	} elseif ( ( $parent_id = wp_get_post_parent_id( $product->id ) ) && has_post_thumbnail( $parent_id ) ) {
		$image = get_the_post_thumbnail_url( $parent_id, $size , $attr);
	} elseif ( $placeholder ) {
		$image = wc_placeholder_img_src( $size );
	} else {
		$image = '';
	}
	return $image;
}
add_filter( 'global_troxler_get_product_image_url', 'global_troxler_get_product_image_url', 10 );

function global_troxler_get_product_thumbnail( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
	global $post, $product;

	$imageURL = '';
	if ( has_post_thumbnail() )
		$imageURL = get_the_post_thumbnail_url();
	elseif ( wc_placeholder_img_src() )
		$imageURL = wc_placeholder_img_src();

	$queryString = "?";
	if(!empty($_GET))
		$queryString = "&";
	$queryString .= "add-to-cart=".get_the_ID();
	if(!empty($_GET['rental']))
		$queryString .= '&is-rental=true';

	$addToCartLink = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].$queryString;
	$addToCartButton = "<span class=\"fa fa-shopping-cart quick-add-to-cart\" href=\"{$addToCartLink}\"></span>";
	if($product->product_type == 'variable'){
		// $addToCartLink .= GetDefaultVariationString();
		$addToCartButton = '';
	}

	$productID = get_the_ID();
	$image = <<<HTML
		<div class="product-image-container">
			<div class="product-image" style="background-image: url('{$imageURL}');"></div>
			<div class="overlay">
				<div class="options">
					<span class="fa fa-search yith-wcqv-button" data-product_id="{$productID}"></span>
					{$addToCartButton}
				</div>
			</div>
		</div>
HTML;

	echo $image;
}

function GetDefaultVariationString()
{
	global $product;

	$string = '';
	// $variationAttributes = $product->get_variation_default_attributes();

	// if(!empty($variationAttributes)){
	// 	$attributeStrings = [];
	// 	foreach($variationAttributes as $key=>$value){
	// 		$attributeStrings[] = 'attribute_'.$key.'='.$value;
	// 	}
	// 	$string .= '&'.implode('&', $attributeStrings);
	// } else {
		$variations = $product->get_available_variations();
		if(!empty($variations)){
			$default = $variations[0];
			$attributes = $default['attributes'];

			$attributeStrings = [];
			foreach($attributes as $key=>$value){
				$attributeStrings[] = $key.'='.$value;
			}
			$string .= '&variation_id='.$default['variation_id'].'&'.implode('&', $attributeStrings);
		}
	// }
	return $string;
}

add_action( 'woocommerce_before_shop_loop_item_title', 'global_troxler_get_product_thumbnail', 10 );

function global_troxler_template_loop_product_title() {
	echo '<p class="title">' . get_the_title() . '</p>';
}

add_action( 'woocommerce_shop_loop_item_title', 'global_troxler_template_loop_product_title', 10 );

// Breadcrumbs
add_filter( 'woocommerce_breadcrumb_defaults', 'global_troxler_woocommerce_breadcrumbs' );
function global_troxler_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &gt; ',
            'wrap_before' => '<div class="columns small-12"><nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav></div>',
            'before'      => '<span>',
            'after'       => '</span>',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}

function global_troxler_products_per_page_label() {
	echo '<label for="ppp">View</label>';
}

add_action( 'wppp_before_dropdown', 'global_troxler_products_per_page_label', 10 );

// Products per page
function ProductsPerPageText( $string, $value ) {
    // (maybe) modify $string
    return '%s';
}
add_filter( 'wppp_ppp_text', 'ProductsPerPageText', 10, 3 );

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

add_filter( 'woocommerce_product_tabs', 'global_troxler_edit_default_tabs', 98 );

function global_troxler_edit_default_tabs( $tabs ) {
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] ); 			// Remove the additional_information tab

    return $tabs;

}

//********************** Custom Product Tabs *****************************//

add_filter( 'woocommerce_product_tabs', 'GlobalTroxlerRemoveDescription' );
function GlobalTroxlerRemoveDescription( $tabs ){
	global $product, $post;
	$content = trim(str_replace('&nbsp;','',strip_tags($product->post->post_content)));

	if(empty($content)){
		unset($tabs['description']);
	}
	return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'global_troxler_specifications_tab' );
function global_troxler_specifications_tab( $tabs ) {
	global $product, $post;

	$specifications = get_field('specifications', $post->ID);

	if(!empty($specifications)){
		$tabs['specifications'] = array(
			'title' 	=> __( 'Specifications', 'global_troxler' ),
			'priority' 	=> 50,
			'callback' 	=> 'global_troxler_specifications_tab_content'
		);
	}

	return $tabs;

}
function global_troxler_specifications_tab_content() {
	wc_get_template( 'single-product/tabs/specifications.php' );
}

add_filter( 'woocommerce_product_tabs', 'global_troxler_product_manual_tab' );
function global_troxler_product_manual_tab( $tabs ) {
	global $product, $post;

	$productManual = get_field('manual-description', $post->ID);
	$productManualFile = get_field('files', $post->ID);

	if(!empty($productManual) || !empty($productManualFile)){
		$tabs['product-manual'] = array(
			'title' 	=> __( 'Product Files', 'global_troxler' ),
			'priority' 	=> 50,
			'callback' 	=> 'global_troxler_product_manual_tab_content'
		);
	}

	return $tabs;

}
function global_troxler_product_manual_tab_content() {
	wc_get_template( 'single-product/tabs/manual.php' );
}

add_filter( 'woocommerce_product_tabs', 'global_troxler_accessories_tab' );
function global_troxler_accessories_tab( $tabs ) {
	global $product, $post;

	if(!empty($product->get_cross_sells())){
		$tabs['accessories'] = array(
			'title' 	=> __( 'Accessories', 'global_troxler' ),
			'priority' 	=> 60,
			'callback' 	=> 'global_troxler_accessories_tab_content'
		);
	}
	return $tabs;

}
function global_troxler_accessories_tab_content() {
	wc_get_template( 'single-product/tabs/accessories.php' );
}

add_filter( 'woocommerce_product_tabs', 'global_troxler_video_tab' );
function global_troxler_video_tab( $tabs ) {
	global $product, $post;

	$productVideo = get_field('product-video', $post->ID);
	if(!empty($productVideo)){
		$tabs['video'] = array(
			'title' 	=> __( 'Video', 'global_troxler' ),
			'priority' 	=> 70,
			'callback' 	=> 'global_troxler_video_tab_content'
		);
	}

	return $tabs;

}
function global_troxler_video_tab_content() {
	wc_get_template( 'single-product/tabs/video.php' );
}
//********************** END Custom Product Tabs *****************************//

//************************** Advanced Search *********************************//

// add_filter('posts_search', 'CustomStandardSearch');

// function CustomStandardSearch($search)
// {
// 	$searchText = $_GET['s'];
// 	$search = "AND (((wp_posts.post_title LIKE '%{$searchText}%') OR (wp_posts.post_excerpt LIKE '%{$searchText}%') OR (wp_posts.post_content LIKE '%{$searchText}%'))) AND (wp_posts.post_password = '')";
// 	return $search;
// }

function global_troxler_advanced_search() {
	wc_get_template( 'advanced-search.php' );
}
add_shortcode( 'global_troxler_advanced_search', 'global_troxler_advanced_search' );

add_filter('woocommerce_product_query_meta_query', 'global_troxler_meta_queries');

function global_troxler_meta_queries($meta_query)
{
	// $meta_query['title'] = global_troxler_title_meta_query();
	$meta_query['sku'] = global_troxler_sku_meta_query();
	$meta_query['is_rental'] = global_troxler_is_rental_meta_query();

	return $meta_query;
}

function global_troxler_sku_meta_query()
{
	return isset( $_GET['sku'] ) ? array(
			'key'           => '_sku',
			'value'         => $_GET['sku'],
			'compare'       => 'LIKE'
		) : array();
}

function global_troxler_is_rental_meta_query()
{
	return isset( $_GET['rental'] ) ? array(
			'key'           => 'is_rental',
			'value'         => $_GET['rental'],
			'compare'       => '='
		) : array();
}

function search_product_title($where, &$wp_query)
{
	global $wpdb;

    if(isset($_GET['title']) && $search_term = $_GET['title']){
        /*using the esc_like() in here instead of other esc_sql()*/
        $search_term = $wpdb->esc_like($search_term);
        $search_term = ' \'%' . $search_term . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE '.$search_term;
    }

    return $where;
}

add_filter( 'posts_where', 'search_product_title', 10, 2);

function search_product_short_description($where, &$wp_query)
{
	global $wpdb;

    if(isset($_GET['short_description']) && $search_term = $_GET['short_description']){
        /*using the esc_like() in here instead of other esc_sql()*/
        $search_term = $wpdb->esc_like($search_term);
        $search_term = ' \'%' . $search_term . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_excerpt LIKE '.$search_term;
    }

    return $where;
}

add_filter( 'posts_where', 'search_product_short_description', 10, 2);

function search_product_description($where, &$wp_query)
{
	global $wpdb;

    if(isset($_GET['description']) && $search_term = $_GET['description']){
        /*using the esc_like() in here instead of other esc_sql()*/
        $search_term = $wpdb->esc_like($search_term);
        $search_term = ' \'%' . $search_term . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_content LIKE '.$search_term;
    }

    return $where;
}

add_filter( 'posts_where', 'search_product_description', 10, 2);

function global_troxler_advanced_search_title($page_title)
{
	if(!empty($_GET['title']) || !empty($_GET['short_description']) || !empty($_GET['description']) || !empty($_GET['sku']))
		return 'Advanced Search';

	if(!empty($_GET['rental']))
		return 'Rentals';

	return $page_title;
}

add_filter('woocommerce_page_title', 'global_troxler_advanced_search_title');

// Add post meta to standard search
// function cf_search_join( $join ) {
//     global $wpdb;

//     if ( is_search() ) {
//         $join .=' LEFT JOIN '.$wpdb->postmeta. ' AS search_meta ON '. $wpdb->posts . '.ID = search_meta.post_id ';
//         $join .=' LEFT JOIN '.$wpdb->posts. ' AS variations ON '. $wpdb->posts .'.ID = variations.post_parent ';
//         $join .=' LEFT JOIN '.$wpdb->postmeta. ' AS variation_search_meta ON variations.ID = variation_search_meta.post_id AND variation_search_meta.meta_key = "_sku" ';
//     }

//     return $join;
// }
// add_filter('posts_join', 'cf_search_join' );

// function cf_search_where( $where ) {
//     global $pagenow, $wpdb;

//     if ( is_search() ) {
//         $where = preg_replace(
//             "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
//             "(".$wpdb->posts.".post_title LIKE $1) OR (search_meta.meta_value LIKE $1) OR (variation_search_meta.meta_value LIKE $1)", $where );

//         $where = preg_replace(
//             "/OR \(\s*post_excerpt\s+LIKE\s*(\'[^\']+\')\s*\)/",
//             " ", $where );
//     }

//     return $where;
// }
// add_filter( 'posts_where', 'cf_search_where', 100 );

// function cf_search_distinct( $where ) {
//     global $wpdb;

//     if ( is_search() ) {
//         return "DISTINCT";
//     }

//     return $where;
// }
// add_filter( 'posts_distinct', 'cf_search_distinct' );

//************************** END Advanced Search *********************************//

//***********************Search Variation SKUs****************************//

// index WooCommerce product_variation SKUs with the parent post
// function my_searchwp_index_woocommerce_variation_skus( $extra_meta, $post_being_indexed ) {
// 	// we only care about WooCommerce Products
// 	if ( 'product' !== get_post_type( $post_being_indexed ) ) {
// 		return $extra_meta;
// 	}
// 	// retrieve all the product variations
// 	$args = array(
// 		'post_type'       => 'product_variation',
// 		'posts_per_page'  => -1,
// 		'fields'          => 'ids',
// 		'post_parent'     => $post_being_indexed->ID,
// 	);
// 	$product_variations = get_posts( $args );
// 	if ( ! empty( $product_variations ) ) {
// 		// store all SKUs as a Custom Field with a key of 'my_product_variation_skus'
// 		$extra_meta['my_product_variation_skus'] = array();
// 		// loop through all product variations, grab and store the SKU
// 		foreach ( $product_variations as $product_variation ) {
// 			$extra_meta['my_product_variation_skus'][] = get_post_meta( absint( $product_variation ), '_sku', true );
// 		}
// 	}
// 	return $extra_meta;
// }
// add_filter( 'searchwp_extra_metadata', 'my_searchwp_index_woocommerce_variation_skus', 10, 2 );

// function my_searchwp_custom_field_keys_variation_skus( $keys ) {
//   $keys[] = 'my_product_variation_skus';

//   return $keys;
// }

// add_filter( 'searchwp_custom_field_keys', 'my_searchwp_custom_field_keys_variation_skus', 10, 1 );

//***********************END Search Variation SKUs****************************//

//************************** Breadcrumbs *********************************//

function global_troxler_modify_breadcrumbs($breadcrumbs, $wc_breadcrumbs)
{
	global $post;
	if(!is_single($GLOBALS['wp_query']->get_queried_object()))
		return;

	$this_category = get_category( $GLOBALS['wp_query']->get_queried_object() );

	if(empty($this_category->term_id) && !empty($post->ID)){
		if($terms = wc_get_product_terms( $post->ID, 'product_cat', array( 'orderby' => 'parent', 'order' => 'DESC' ) ) ) {
			$this_category = $terms[0];
		}
	}
	// $terms = wc_get_product_terms( $post->ID, 'product_cat', array( 'orderby' => 'parent', 'order' => 'DESC' ) )
	$groupingCategories = [];
	if(!empty($this_category->term_id)){
		$ancestors = get_ancestors( $this_category->term_id, 'product_cat' );


		foreach($ancestors as $ancestor){
			$ancestor = get_term( $ancestor, 'product_cat' );
			$term_grouping = get_option( "taxonomy_$ancestor->term_id" );
	        $grouping_value = esc_attr( $term_grouping['custom_term_grouping'] );

	        if($grouping_value == "Yes")
	        	$groupingCategories[] = $ancestor->name;
		}
	}
	$removeFromCrumbs = [];
	foreach($breadcrumbs as $key=>$crumb){
		if($crumb[0] == 'Shop')
			$removeFromCrumbs[] = $key;
		if(in_array($crumb[0], $groupingCategories))
			$removeFromCrumbs[] = $key;
	}

	if(!empty($removeFromCrumbs)){
		foreach($removeFromCrumbs as $remove)
			unset($breadcrumbs[$remove]);
		$breadcrumbs = array_values($breadcrumbs);

	}

	if(!empty($_GET['title']) || !empty($_GET['short_description']) || !empty($_GET['description']) || !empty($_GET['sku']))
		$breadcrumbs[] = ['Advanced Search'];

	if(!empty($_GET['rental']))
		$breadcrumbs[] = ['Rentals'];

	return $breadcrumbs;
}

add_filter('woocommerce_get_breadcrumb', 'global_troxler_modify_breadcrumbs', 10, 3);

//************************** END Breadcrumbs *********************************//

//************************* Remove Checkout Fields *******************************//

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
     unset($fields['order']['order_comments']);
     // unset($fields['billing']['billing_address_1']);
     // unset($fields['billing']['billing_address_2']);
     // unset($fields['billing']['billing_state']);
     // unset($fields['billing']['billing_city']);
     // unset($fields['billing']['billing_postcode']);
     // unset($fields['billing']['billing_country']);
     $fields['billing']['billing_company']['required'] = true;

     return $fields;
}

//************************* END Remove Checkout Fields *******************************//

//************************* Requrie Company in Address *******************************//

add_filter( 'woocommerce_default_address_fields' , 'GTDefaultAddressFields' );

function GTDefaultAddressFields($fields){
	$fields['company']['required'] = true;
	return $fields;
}

//************************* END Require Company in Address *******************************//

//********************************* Quick View ***************************************//

remove_action( 'yith_wcqv_product_image', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'yith_wcqv_product_image', 'global_troxler_quick_view_before_image', 10 );
add_action( 'yith_wcqv_product_image', 'global_troxler_quick_view_after_image', 30 );
add_action( 'yith_wcqv_product_summary', 'global_troxler_quick_view_after_summary', 35 );
remove_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_meta', 30 );
remove_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_price', 15 );

function global_troxler_quick_view_before_image() {

    echo '<div class="columns medium-3">';
}

function global_troxler_quick_view_after_image() {

    echo '</div><div class="columns medium-9">';
}

function global_troxler_quick_view_after_summary() {

    echo '</div>';
}

add_filter( 'wc_get_template', 'global_troxler_quick_view_content_template', 99 );

function global_troxler_quick_view_content_template( $template ) {

	$test = 'yith-quick-view-content.php';
	if(substr_compare($template, $test, strlen($template)-strlen($test), strlen($test)) === 0)
		return get_template_directory().'/yith-woocommerce-quick-view/yith-quick-view-content.php';

	return $template;
}

//************************* END Quick View *******************************//


//********************************* Add Name to Customer Details in Emails ***************************************//

function AddNameToEmailCustomerDetailsFields($fields, $sent_to_admin, $order)
{
	if ( $order->billing_first_name || $order->billing_last_name ) {
		$name = '';
		if(!empty($order->billing_first_name))
			$name .= "{$order->billing_first_name} ";
		if(!empty($order->billing_last_name))
			$name .= "{$order->billing_last_name}";

		$fields['billing_name'] = array(
			'label' => __( 'Name', 'woocommerce' ),
			'value' => wptexturize( $name )
		);
	}

	return $fields;
}

add_filter('woocommerce_email_customer_details_fields', 'AddNameToEmailCustomerDetailsFields', 10, 3);

//********************************* END Add Name to Customer Details in Emails ***************************************//

//Store the custom field
// add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_custom_data_vase', 10, 2 );
// function add_cart_item_custom_data_vase( $cart_item_meta, $product_id ) {
// 	global $woocommerce;
// 	// $cart_item_meta['test_field'] = $_POST['test_field'];
// 	$cart_item_meta['test_field'] = 'testo';
// 	return $cart_item_meta;
// }

// //Get it from the session and add it to the cart variable
// function get_cart_items_from_session( $item, $values, $key ) {
//     if ( array_key_exists( 'test_field', $values ) )
//         $item[ 'mmCentre' ] = $values['test_field'];
//     return $item;
// }
// add_filter( 'woocommerce_get_cart_item_from_session', 'get_cart_items_from_session', 1, 3 );

//********************************* Sensei ***************************************//
if(function_exists("Sensei")){
	global $sensei_media_attachments;

	remove_action('sensei_course_content_inside_before', array( Sensei()->course, 'course_image' ) ,10, 1 );
	remove_action ( 'sensei_archive_before_course_loop' , array( 'Sensei_Course', 'course_archive_sorting' ) );
	remove_action ( 'sensei_archive_before_course_loop' , array( 'Sensei_Course', 'course_archive_filters' ) );
	remove_action ( 'sensei_single_course_content_inside_before' , array( 'Sensei_Course', 'the_title' ) );
	remove_action( 'sensei_course_single_lessons', array( $sensei_media_attachments, 'display_attached_media' ), 9 );
	remove_action( 'sensei_course_results_content_inside_before', array( Sensei()->course,'course_image') );

	add_action('sensei_course_content_inside_before', 'GTCourseImage', 4);
	add_action('sensei-single-course-left-column', 'GTCourseImage', 5);
	add_action( 'sensei_single_course_content_inside_after', array( $sensei_media_attachments, 'display_attached_media' ), 1 );
}

function GTCourseImage()
{
	$imageURL = GetCourseImageURL();
	$link = get_permalink();
	$image = <<<HTML
		<a href="{$link}">
			<div class="product-image-container">
				<div class="product-image" style="background-image: url('{$imageURL}');"></div>
			</div>
		</a>
HTML;

	echo $image;
}

function GetCourseImageURL()
{
	$imageURL = '';
	if ( has_post_thumbnail() )
		$imageURL = get_the_post_thumbnail_url();
	elseif ( wc_placeholder_img_src() )
		$imageURL = wc_placeholder_img_src();

	return $imageURL;
}

add_filter('sensei_course_loop_content_class', 'GTAddCourseLoopClass');

function GTAddCourseLoopClass($extra_classes)
{
	$extra_classes[] = 'columns';
	$extra_classes[] = 'medium-3';
	$extra_classes[] = 'product';
	$extra_classes[] = 'product-item';
	return $extra_classes;
}

add_filter('sensei_the_title_html_tag',function(){ return 'p';});
add_filter('sensei_the_title_classes',function(){ return 'title';});

//********************************* END Sensei ***************************************//

function GTGravityFormExists($form_name){

  $forms = GFFormsModel::get_forms();
	foreach($forms as &$form){
		if ($form->title == $form_name) {
			return true;
		}
	}
  return false;
}

add_filter( 'wp_nav_menu_items', 'GTChangeLogin', 10, 2 );
function GTChangeLogin( $items, $args ) {
    if(is_user_logged_in() && ($args->theme_location == 'top-menu' || $args->theme_location == 'top-footer-menu')){
    	$items = str_replace('Client Login', 'My Account', $items);
    }
    return $items;
    // if (is_user_logged_in() && $args->theme_location == 'main-menu') {
    //     $items .= '<li><a href="'. wp_logout_url(get_permalink()) .'">Log Out</a></li>';
    // }
    // elseif (!is_user_logged_in() && $args->theme_location == 'main-menu') {
    //     $items .= '<li><a href="'. get_site_url() . '/my-courses">Log In</a></li>';
    // }
    // return $items;
}

function CustomPagination($totalPages, $currentPage){
	$baseURL = get_site_url();
	$prevPageButton = "";
	$nextPageButton = "";
	if($currentPage != 1){
		$prevPage = $currentPage-1;
		$prevPageButton = "<li><a class='prev page-numbers' href='{$baseURL}/blog/page/{$prevPage}'><i class='fa fa-chevron-left'></i></a></li>";
	}
	if($currentPage != $totalPages){
		$nextPage = $currentPage+1;
		$nextPageButton = "<li><a class='prev page-numbers' href='{$baseURL}/blog/page/{$nextPage}'><i class='fa fa-chevron-right'></i></a></li>";
	}
	$pageNumbers = "";
	for($i = $currentPage-2;$i <= $currentPage+2; $i++){
		if($i > 0 && $i <= $totalPages){
			if($i == $currentPage)
				$pageNumbers .= "<li><span class='page-numbers current'>{$i}</span></li>";
			else
				$pageNumbers .= "<li><a class='page-numbers' href='{$baseURL}/blog/page/{$i}'>{$i}</a></li>";
		}
	}
	$pagination = <<<HTML
		<div class="columns small-12 flex-container align-center pagination">
			<nav class="woocommerce-pagination">
				<ul class="page-numbers">
					{$prevPageButton}
					{$pageNumbers}
					{$nextPageButton}
				</ul>
			</nav>
		</div>
HTML;

	return $pagination;
}

function GTEnqueueAdminScript( $hook ) {
	if($hook != 'product_page_to-interface-product')
		return;

	// wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js');
    wp_enqueue_script( 'troxler-admin', get_template_directory_uri() . '/js/admin.js', ['jquery'] );
}

add_action('admin_enqueue_scripts', 'GTEnqueueAdminScript');

function wpa104537_filter_products_by_featured_status() {

     global $typenow, $wp_query;

    if ($typenow=='product') :


        // Featured/ Not Featured
        $output = "<select name='featured_status' id='dropdown_featured_status'>";
        $output .= '<option value="">'.__( 'Show All Featured Statuses', 'woocommerce' ).'</option>';

        $output .="<option value='featured' ";
        if ( isset( $_GET['featured_status'] ) ) $output .= selected('featured', $_GET['featured_status'], false);
        $output .=">".__( 'Featured', 'woocommerce' )."</option>";

        $output .="<option value='normal' ";
        if ( isset( $_GET['featured_status'] ) ) $output .= selected('normal', $_GET['featured_status'], false);
        $output .=">".__( 'Not Featured', 'woocommerce' )."</option>";

        $output .="</select>";

        echo $output;
    endif;
}

add_action('restrict_manage_posts', 'wpa104537_filter_products_by_featured_status');

function wpa104537_featured_products_admin_filter_query( $query ) {
    global $typenow;

    if ( $typenow == 'product' ) {

        // Subtypes
        if ( ! empty( $_GET['featured_status'] ) ) {
            if ( $_GET['featured_status'] == 'featured' ) {
                $query->query_vars['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'slug',
                    'terms'    => 'featured',
                );
            } elseif ( $_GET['featured_status'] == 'normal' ) {
                $query->query_vars['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'slug',
                    'terms'    => 'featured',
                    'operator' => 'NOT IN',
                );
            }
        }

    }

}
add_filter( 'parse_query', 'wpa104537_featured_products_admin_filter_query' );


function custom_wc_ajax_variation_threshold( $qty, $product ) {
	return 50;
}

add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 10, 2 );

function woocommerce_support_troxler() {
  add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'woocommerce_support_troxler' );
