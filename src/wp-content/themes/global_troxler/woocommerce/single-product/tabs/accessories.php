<?php
/**
 * Additional Information tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/additional-information.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

		<ul class="row product-list no-bullet">
			<?php $args = array(
			    'post_type' => 'product',
			    'posts_per_page' => 12 ,
			    'post__in'       => array_merge( array( 0 ), $product->get_cross_sells() ),
			    'orderby' => 'rand'
			);

			$featured_query = new WP_Query( $args );
			if ($featured_query->have_posts()) {

			    while ( $featured_query->have_posts() ) {
			    	$featured_query->the_post(); ?>

			        <li <?php post_class(["columns medium-3 small-12 product-item"]); ?>>
			        	<?php wc_get_template_part( 'content', 'product' ); ?>
			        </li>

			<?php }

			}

			wp_reset_query();
?>
		</ul>
