$(document).ready(function(){
	'use strict';

	$(document).foundation();
	
	// $('.mobile-toggle').click(function(){
	// 	$(this).toggleClass('open');
	// 	if ($('.mobile-toggle').hasClass('open')) {
	// 		$('.mobile-toggle').find('.fa').removeClass('fa-bars').addClass('fa-times');
	// 		$('.mobile-nav').slideDown();
	// 	} else {
	// 		$('.mobile-toggle').find('.fa').addClass('fa-bars').removeClass('fa-times');
	// 		$('.mobile-nav').slideUp();
	// 	}
	// });



	$('.homepage-carousel').slick({
		dots: true,
		appendArrows: $('#carousel-controls .arrows'),
		appendDots: $('#carousel-controls'),
		prevArrow: '<a class="prev-arrow"><i class="fa fa-angle-left"></i></a>',
		nextArrow: '<a class="next-arrow"><i class="fa fa-angle-right"></i></a>',
		swipeToSlide: true,
		adaptiveHeight: true,
		autoplay: true,
		autoplaySpeed: 2000,
		speed: 1000,
		responsive: [
			{
			  breakpoint: 640,
			  settings: {
				arrows: false
			  }
			}
		  ]
	});

	var productsNav = $(".full-menu li a:contains('Products')");
	var mobileProductsNav = $(".mobile-nav li a:contains('Products')");

	var categoriesNav = $('#category-dropdown').detach();
	var mobileCategoriesNav = categoriesNav.clone();

	// productsNav.parent().parent().attr('data-dropdown-menu','');
	productsNav.parent().append(categoriesNav);
	mobileProductsNav.parent().append(mobileCategoriesNav);
	// productsNav.parent().attr('data-dropdown','category-dropdown');
	// productsNav.parent().attr('data-options','is_hover:true; hover_timeout:5000');
	
	$('#menu-main-menu-1 > li').hover(function(){
		$(this).find('.sub-menu').show();
	}, function(){
		$(this).find('.sub-menu').hide();
	})

	$('.widget_global_troxler_filters .parent-category').click(function(){
		$(this).parent().toggleClass('closed');
	});

	$('.variations button').click(function(){
		return false;
	});
	
	$(document).on('click', '.add-quantity', function(event){
		event.preventDefault();
		var quantity = parseInt($('.qty').val());
		quantity++;
		$('.qty').val(quantity);
		return false;
	});

	$(document).on('click', '.remove-quantity', function(event){
		event.preventDefault();
		var quantity = parseInt($('.qty').val());
		quantity--;
		$('.qty').val(quantity);
		return false;
	});

	$('.quick-add-to-cart').click(function(event){
		event.stopImmediatePropagation();
		// console.log($(this).attr('href'));
		window.location = $(this).attr('href');
		return false;
	});

	$(document).on('click', '.rent-product', function(event){
		event.preventDefault();
		$('.add-to-cart').append('<input type="hidden" name="is-rental" value="true" />');
		$(this).closest('form').submit();
		return false;
	});

	$(document).on('click', '.lease-product', function(event){
		event.preventDefault();
		$('.add-to-cart').append('<input type="hidden" name="is-lease" value="true" />');
		$(this).closest('form').submit();
		return false;
	})

	$('#menu-main-menu-1 > li > a').click(function(){
		return false;
	});

	$('#payment_method').change(function(){
		if($(this).val() == 'Other')
			$('#other_payment_method_field').show();
		else
			$('#other_payment_method_field').hide();
	});

	$('#preferred_carrier').change(function(){
		if($(this).val() == 'Other')
			$('#other_preferred_carrier_field').show();
		else
			$('#other_preferred_carrier_field').hide();
	});

	$(document).on('click', '.add-button', function(event){
		event.preventDefault();
		var addon = $('#product-addons').val();
		var alreadyAdded = false;
		$('#addon-list li').each(function(){
			if($(this).attr('data-addon') == addon)
				alreadyAdded = true;
		})
		if(!alreadyAdded){
			var addonListEle = $('<li><i class="fa fa-times-circle"></i> '+addon+'</li>');
			addonListEle.attr('data-addon', addon);
			addonListEle.find('i').click(function(){
				$(this).parent().remove();
				UpdateAddons();
			})
			$('#addon-list').append(addonListEle);
			UpdateAddons();
		}
	});

	function UpdateAddons(){
		var addons = [];
		$('#addon-list li').each(function(){
			addons.push($(this).attr('data-addon'));
		});
		$('.add-to-cart input[name="addons"]').remove();
		if(addons.length > 0)
			$('.add-to-cart').append('<input type="hidden" name="addons" value="'+addons.join()+'" />');
	}

	$('input[name="select-all"]').change(function(){
		if($(this).is(':checked')){

			$('.select-categories').find('input').each(function(){
				$(this).prop('checked',true);
			})
		} else {
			$('.select-categories').find('input').each(function(){
				$(this).prop('checked', false);
			})
		}
	})
});