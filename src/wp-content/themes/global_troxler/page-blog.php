<?php get_header(); ?>
<section id="content" role="main" class="row">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('medium-12 columns'); ?>>
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<section class="entry-content blog">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
<?php the_content(); ?>

<?php 

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$postQuery = new WP_Query(
	array(
		'post_type'=>'post',
		'post_status'=>'publish',
		'posts_per_page'=>10,
		'paged'=>$paged
	)
);
?>

<ul class="no-bullet columns small-12 blog-list">
<?php if ( $postQuery->have_posts() ) : while ( $postQuery->have_posts() ) : $postQuery->the_post(); ?>
	<li class="row">
		<div class="columns small-12 medium-2">
			<div class="product-image-container">
				<div class="product-image" style="background-image: url('<?=the_post_thumbnail_url();?>');"></div>
			</div>
		</div>
		<div class="columns small-12 medium-10">
			<h4><a href="<?=the_permalink();?>"><?=the_title();?></a></h4>
			<div class="excerpt"><?=the_excerpt();?></div>
		</div>
	</li>
<?php endwhile; ?>
	<?= CustomPagination($postQuery->max_num_pages, $paged);?>
<?php else: ?>
  <li><p><?php _e('Sorry, no posts matched your criteria.'); ?></p></li>
<?php endif; ?>
</ul>



</section>
</article>
<?php endwhile; endif; ?>
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>
