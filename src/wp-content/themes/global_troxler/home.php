<?php get_header(); ?>
<div class="row">
	<?php wc_print_notices(); ?>
</div>
<div class="row collapse">
	<div class="columns small-12">
		<section id="carousel">
			<div class="homepage-carousel">
				<?php 
					$args = array('post_type' => 'gt_carousel_image');
					$carouselQuery = new WP_Query($args);
					while ($carouselQuery->have_posts()) {
						$carouselQuery->the_post();
						$link = get_field('link');
						$fullWidth = get_field('full_width');
				 		if($fullWidth){ ?>
							<div class="slide full-width"><?=!empty($link) ? "<a href='{$link}'>" : '';?><img src="<?= get_the_post_thumbnail_url(); ?>" /><?=!empty($link) ? '</a>' : '';?></div>
						<?php 
						} else { 
						?>
							<div class="slide">
								<?=!empty($link) ? "<a href='{$link}'>" : '';?>
									<div class="slide-content">
										<img src="<?= get_the_post_thumbnail_url(); ?>" />
										<div class="slide-text">
											<p class="title"><?=the_title();?></p>
											<div><?=the_content();?></div>
										</div>
									</div>
								<?=!empty($link) ? '</a>' : '';?>
							</div>
					<?php 
						}
					} 
					?>
		  	</div>
		  	<div class="row" id="carousel-controls">
		  		<div class="arrows columns medium-12"></div>
		  	</div>
		</section>
	</div>
</div>
<section id="content" role="main" class="row">
	<section class="product-lists columns medium-12">
		<h2 class="entry-title">Featured Products</h2>
		<ul class="row product-list no-bullet">
		<?php $args = array(  
			    'post_type' => 'product',  
			    'posts_per_page' => 4,
			    'orderby' => 'rand',
			    'tax_query' => [
			    	[
			    		'taxonomy' => 'product_visibility',
			    		'field'    => 'slug',
			    		'terms'    => 'featured',
			    		'operator' => 'IN'
			    	]
			    ]
			);  
			  
			$featured_query = new WP_Query( $args );  
			if ($featured_query->have_posts()) {   
			  
			    while ( $featured_query->have_posts() ) { 
			    	$featured_query->the_post(); ?>
			      
			        <li <?php post_class(["columns medium-3 small-12 product-item"]); ?>>
			        	<?php wc_get_template_part( 'content', 'product' ); ?>
			        </li>  
			          
			<?php }  
			      
			}  
			  
			wp_reset_query();
?>
		</ul>
		
		<h2 class="entry-title">Sale Products</h2>
		<ul class="row product-list no-bullet">
			<?php $args = array(  
			    'post_type' => 'product',   
			    'posts_per_page' => 4 ,
			    'post__in'       => array_merge( array( 0 ), wc_get_product_ids_on_sale() ),
			    'orderby' => 'rand' 
			);  
			  
			$featured_query = new WP_Query( $args );  
			if ($featured_query->have_posts()) {   
			  
			    while ( $featured_query->have_posts() ) { 
			    	$featured_query->the_post(); ?>
			      
			        <li <?php post_class(["columns medium-3 small-12 product-item"]); ?>>
			        	<?php wc_get_template_part( 'content', 'product' ); ?>
			        </li>  
			          
			<?php }  
			      
			}  
			  
			wp_reset_query();
?>
		</ul>
	</section>
</section>
<?php get_footer(); ?>