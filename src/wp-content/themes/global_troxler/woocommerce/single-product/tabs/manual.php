<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

?>

<?php 
	$productManual = get_field('manual-description', $post->ID);
	$productFiles = get_field('files', $post->ID);

	if(!empty($productManual))
		echo $productManual;

	if(!empty($productFiles)){
		echo '<ul class="no-bullet files-list">';
		foreach($productFiles as $file){
			echo "<li><a href='{$file['file']}' class='product-manual-link' target='_blank'><i class='fa fa-file-pdf-o'></i> {$file['name']}</a></li>";
		}
		echo '</ul>';
	}
 ?>