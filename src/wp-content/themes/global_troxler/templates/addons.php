<div class="row addons">
	<div class="columns medium-6 small-10">
		<label>Addons</label>
		<div class="input-group">
			<select id="product-addons" class="input-group-field small">
				<?php foreach($addons as $addon){ ?>
					<option value="<?=$addon['name'];?>"><?=$addon['name'];?></option>
				<?php } ?>
			</select>
			<div class="input-group-button">
				<button class="button small select-button"><span class="fa fa-chevron-down"></span></button>
			</div>
			
		</div>
	</div>
	<div class="columns medium-6 small-2">
		<button class="button add-button">Add</button>
	</div>
	<div class="columns medium-6 small-12">
		<ul class="no-bullet" id="addon-list">
		</ul>
	</div>
</div>