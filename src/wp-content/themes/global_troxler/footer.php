<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
	<nav role="navigation" class="blue-bar hide-for-small-only" id="top-footer-bar">
		<div class="row align-middle">
		<?php wp_nav_menu( array( 'theme_location' => 'top-footer-menu' , 'container_class' => '', 'menu_class' => 'menu') ); ?>
		<ul class="menu">
			<li><a class="subscribe-button" data-open="subscribe-modal"><?= __('Subscribe to Newsletter', 'woocommerce');?></a></li>
			<li><a class="no-arrow quote-cart" href="<?=get_site_url();?>/cart"><?= __('Cart', 'woocommerce');?> <i class="fa fa-shopping-cart"></i></a></li>
		</ul>
		</div>
	</nav>
	<div class="reveal" id="subscribe-modal" data-reveal>
		<h3>Subscribe to Newsletter</h3>
		<?= do_shortcode('[gravityform id=1 title=false description=false ajax=true]');?>
	</div>
	<div id="middle-footer-bar">
		<div class="row">
			<div class="columns medium-12 small-12">
				<p><a href="<?=get_site_url();?>/contact">Contact Us ></a></p>
			</div>
		</div>
		<div class="row contact-list">
			<div class="columns medium-3 small-12 contact-item">
				<i class="fa fa-mobile"></i>
				<p>Call us:  <a href="tel:18663009012">+1-866-300-9012</a></p>
			</div>
			<div class="columns medium-3 small-12 contact-item">
				<i class="fa fa-envelope"></i>
				<p>Email Us: <a href="mailto:contact@measur.ca">contact@measur.ca</a></p>
			</div>
			<div class="columns medium-3 small-12">
				<div class="contact-item">
					<i class="fa fa-map-marker"></i>
					<div class="locations">
						<p>Calgary Head Office</p>
						<address>2 - 1540 Hastings Cres. S.E. <br/>Calgary, AB. <br/>T2G 4E1</address><br/>
					</div>
				</div>
				<div class="contact-item">
					<i class="fa fa-map-marker"></i>
					<div class="locations">
						<p>Burnaby Office</p>
						<address>7 - 5850 Byrne Road <br/>Burnaby, BC. <br/>V5J 3J3</address>
					</div>
				</div>
			</div>
			<div class="columns medium-3 small-12">
				<i class="fa fa-linkedin-square"></i>
				<p><a href="https://www.linkedin.com/company/globaltroxler">LinkedIn</a></p>
			</div>
		</div>
					<!-- <div class="columns medium-3 small-12">
						<div class="row">
							<span class="columns small-1 fa fa-mobile"></span> <p class="columns small-11">Call us:  <a href="tel:18663009012">+1-866-300-9012</a></p>
						</div>
					</div>
					<div class="columns medium-3 small-12">
						<div class="row">
							<span class="columns small-1 fa fa-envelope"></span> <p class="columns small-11">Email Us: <a href="mailto:contact@measur.ca">contact@measur.ca</a></p>
						</div>
					</div>
					<div class="columns medium-3 small-12">
						<div class="row">
							<span class="columns small-1 fa fa-map-marker"></span> <address class="columns small-11">#2, 1540 Hastings Cres. S.E. Calgary, AB. T2G 4E1</address>
						</div>
					</div>
					<div class="columns medium-3 small-12">
						<div class="row">
							<div class="columns small-1"><i class="fa fa-linkedin"></i></div> <div class="columns small-11">LinkedIn</div>
						</div>
					</div> -->
					<!-- <div class="columns small-1">
						<ul class="contact-list contact-list-icons">
							<li><span class="fa fa-mobile"></span></li>
							<li><span class="fa fa-envelope"></span></li>
							<li><span class="fa fa-map-marker"></span></li>
						</ul>
					</div>
					<div class="columns small-11">
						<ul class="contact-list contact-list-text">
							<li>Call us to ask any questions:  <a href="tel:18663009012">+1-866-300-9012</a></li>
							<li>Email Us: <a href="mailto:contact@measur.ca">contact@measur.ca</a></li>
							<li>#2, 1540 Hastings Cres. S.E. Calgary, AB. T2G 4E1</li>
						</ul>
					</div> -->
	</div>
	<nav role="navigation" class="blue-bar" id="bottom-footer-bar">
		<div class="row">
			<ul class="menu align-left columns medium-6 small-12">
				<li><img src="<?= get_stylesheet_directory_uri(); ?>/images/small-logo-border.png" alt="Global Troxler" /></li>
				<li class="copyright"><p>Copyright &copy; <?=date('Y');?>. All rights reserved.</p></li>
			</ul>
			<div class="row align-middle align-right columns medium-6 hide-for-small-only">
				<?php wp_nav_menu( array( 'theme_location' => 'bottom-footer-menu' , 'container_class' => '', 'menu_class' => 'menu float-right') ); ?>
			</div>
		</div>
	</nav>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>