<?php

/*
Plugin Name: Global Troxler Products
Description: This will modify woocommerce for the main part of the site
Version: 1.0
Author: Kelly Anderson for William Joseph
Author URI: http://www.williamjoseph.com
*/

add_option('hide-prices', true);

function GTJavascripts()
{
	wp_enqueue_script('customFunctions', get_stylesheet_directory_uri() . '/js/gt-main.js');
}

add_action ('wp_enqueue_scripts', 'GTJavascripts', 2);

// *********** Adding Custom Field to Product Category Page **************** //

// Add term page
function taxonomy_add_new_grouping_field() {
	// this will add the custom grouping field to the add new term page
	?>
	<div class="form-field">
		<label for="term_grouping[custom_term_grouping]"><?php _e( 'Grouping' ); ?></label>
		<select name="term_grouping[custom_term_grouping]" id="term_grouping[custom_term_grouping]">
			<option value="Yes">Yes</option>
			<option value="No">No</option>
		</select>
		<p class="description"><?php _e( 'Choose whether this category is a grouping' ); ?></p>
	</div>
<?php
}
add_action( 'product_cat_add_form_fields', 'taxonomy_add_new_grouping_field', 10, 2 );

// Edit term page
function taxonomy_edit_grouping_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this grouping field. This returns an array
	$term_grouping = get_option( "taxonomy_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_grouping[custom_term_grouping]"><?php _e( 'Grouping'); ?></label></th>
		<td>
			<select name="term_grouping[custom_term_grouping]" id="term_grouping[custom_term_grouping]">
				<?php $value = esc_attr( $term_grouping['custom_term_grouping'] ); ?>
				<option value="Yes" <?=($value == "Yes") ? 'selected' : '';?>>Yes</option>
				<option value="No" <?=($value == "No" || empty($value)) ? 'selected' : '';?>>No</option>
			</select>
			<p class="description"><?php _e( 'Choose whether this category is a grouping' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'product_cat_edit_form_fields', 'taxonomy_edit_grouping_field', 10, 2 );

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_grouping( $term_id ) {
	if ( isset( $_POST['term_grouping'] ) ) {
		$t_id = $term_id;
		$term_grouping = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_grouping'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_grouping'][$key] ) ) {
				$term_grouping[$key] = $_POST['term_grouping'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_grouping );
	}
}  
add_action( 'edited_product_cat', 'save_taxonomy_custom_grouping', 10, 2 );  
add_action( 'create_product_cat', 'save_taxonomy_custom_grouping', 10, 2 );


function add_product_cat_columns($columns){
    $columns['grouping'] = 'Grouping';
    return $columns;
}
add_filter('manage_edit-product_cat_columns', 'add_product_cat_columns');

function add_product_cat_column_content($content,$column_name,$term_id){
    $term= get_term($term_id, 'product_cat');
    switch ($column_name) {
        case 'grouping':
            //do your stuff here with $term or $term_id
        	$term_grouping = get_option( "taxonomy_$term->term_id" );
        	$value = esc_attr( $term_grouping['custom_term_grouping'] );
            $content = (!empty($value)) ? $value : 'No';
            break;
        default:
            break;
    }
    return $content;
}
add_filter('manage_product_cat_custom_column', 'add_product_cat_column_content',10,3);

// *********** END Adding Custom Field to Product Category Page **************** //

// *********** Add Collapsed Option to Product Categories **************** //

// Add term page
function taxonomy_add_new_collapsed_field() {
    // this will add the custom collapsed field to the add new term page
    ?>
    <div class="form-field">
        <label for="term_collapsed[custom_term_collapsed]"><?php _e( 'Collapsed' ); ?></label>
        <select name="term_collapsed[custom_term_collapsed]" id="term_collapsed[custom_term_collapsed]">
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
        <p class="description"><?php _e( 'Choose whether this category is collapsed by default' ); ?></p>
    </div>
<?php
}
add_action( 'product_cat_add_form_fields', 'taxonomy_add_new_collapsed_field', 10, 2 );

// Edit term page
function taxonomy_edit_collapsed_field($term) {
 
    // put the term ID into a variable
    $t_id = $term->term_id;
 
    // retrieve the existing value(s) for this collapsed field. This returns an array
    $term_collapsed = get_option( "taxonomy_$t_id" ); ?>
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_collapsed[custom_term_collapsed]"><?php _e( 'Collapsed'); ?></label></th>
        <td>
            <select name="term_collapsed[custom_term_collapsed]" id="term_collapsed[custom_term_collapsed]">
                <?php $value = esc_attr( $term_collapsed['custom_term_collapsed'] ); ?>
                <option value="Yes" <?=($value == "Yes") ? 'selected' : '';?>>Yes</option>
                <option value="No" <?=($value == "No" || empty($value)) ? 'selected' : '';?>>No</option>
            </select>
            <p class="description"><?php _e( 'Choose whether this category is collapsed by default' ); ?></p>
        </td>
    </tr>
<?php
}
add_action( 'product_cat_edit_form_fields', 'taxonomy_edit_collapsed_field', 10, 2 );

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_collapsed( $term_id ) {
    if ( isset( $_POST['term_collapsed'] ) ) {
        $t_id = $term_id;
        $term_collapsed = get_option( "taxonomy_$t_id" );
        $cat_keys = array_keys( $_POST['term_collapsed'] );
        foreach ( $cat_keys as $key ) {
            if ( isset ( $_POST['term_collapsed'][$key] ) ) {
                $term_collapsed[$key] = $_POST['term_collapsed'][$key];
            }
        }
        // Save the option array.
        update_option( "taxonomy_$t_id", $term_collapsed );
    }
}  
add_action( 'edited_product_cat', 'save_taxonomy_custom_collapsed', 10, 2 );  
add_action( 'create_product_cat', 'save_taxonomy_custom_collapsed', 10, 2 );


function add_product_cat_column_collapsed($columns){
    $columns['collapsed'] = 'Collapsed';
    return $columns;
}
add_filter('manage_edit-product_cat_columns', 'add_product_cat_column_collapsed');

function add_product_cat_column_collapsed_content($content,$column_name,$term_id){
    $term= get_term($term_id, 'product_cat');
    switch ($column_name) {
        case 'collapsed':
            //do your stuff here with $term or $term_id
            $term_collapsed = get_option( "taxonomy_$term->term_id" );
            $value = esc_attr( $term_collapsed['custom_term_collapsed'] );
            $content = (!empty($value)) ? $value : 'No';
            break;
        default:
            break;
    }
    return $content;
}
add_filter('manage_product_cat_custom_column', 'add_product_cat_column_collapsed_content',10,3);

// *********** END Add Collapsed Option to Product Categories **************** //


//********************************* Change Text ***************************************//

function my_text_strings( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'Order Received' :
			$translated_text = __( 'Quote Request Received', 'woocommerce' );
			break;
		case 'The order #%d from %s has been cancelled. The order was as follows:' :
			$translated_text = __( 'The quote request #%d from %s has been cancelled. The quote request was as follows:', 'woocommerce' );
			break;
		case 'Payment for order #%d from %s has failed. The order was as follows:' :
			$translated_text = __( 'Payment for quote request #%d from %s has failed. The quote request was as follows:', 'woocommerce' );
			break;
		case 'You have received an order from %s. The order is as follows:' :
			$translated_text = __( 'You have received a quote request from %s. The quote request is as follows:', 'woocommerce' );
			break;
		case 'Hi there. Your recent order on %s has been completed. Your order details are shown below for your reference:' :
			$translated_text = __( 'Hi there. Your recent quote request on Global Troxler has been received and you can expect to receive your quote within 48 hours. Your quote request details are shown below for your reference:', 'woocommerce' );
			break;
		case 'An order has been created for you on %s. To pay for this order please use the following link: %s' :
			$translated_text = __( 'A quote request has been created for you on %s. To pay for this quote request please use the following link: %s', 'woocommerce' );
			break;
		case 'Hello, a note has just been added to your order:' :
			$translated_text = __( 'Hello, a note has just been added to your quote request:', 'woocommerce' );
			break;
		case 'For your reference, your order details are shown below.' :
			$translated_text = __( 'For your reference, your quote request details are shown below.', 'woocommerce' );
			break;
		case 'Your order has been received and is now being processed. Your order details are shown below for your reference:' :
			$translated_text = __( 'Your quote request has been received and is now being processed. Your quote request details are shown below for your reference:', 'woocommerce' );
			break;
		case 'Order #%s' :
			$translated_text = __( 'Quote Request #%s', 'woocommerce' );
			break;
		case 'Cart' :
			$translated_text = __( 'Quote Cart', 'woocommerce' );
			break;
		case 'SKU:' :
			$translated_text = __( 'Product Number:', 'woocommerce' );
			break;
		case 'Add to Cart' :
			$translated_text = __( 'Add to Quote Cart', 'woocommerce' );
			break;
		case 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.' :
			$translated_text = __( 'Unfortunately your quote request cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' );
			break;
		case 'Thank you. Your order has been received.' :
			$translated_text = __( 'Thank you. Your quote request has been received.', 'woocommerce' );
			break;
		case 'SKU' :
			$translated_text = __( 'Product Number', 'woocommerce' );
			break;
		case 'Stock Keeping Unit' :
			$translated_text = __( 'Product Number', 'woocommerce' );
			break;
        case 'Your order' :
            $translated_text = __( 'Your quote', 'woocommerce' );
            break;
        case 'From your account dashboard you can view your %1$srecent orders%2$s, manage your %3$sshipping and billing addresses%2$s and %4$sedit your password and account details%2$s.':
            $translated_text = __( 'From your account dashboard you can manage your %3$sshipping and billing addresses%2$s and %4$sedit your password and account details%2$s.', 'woocommerce');
            break;
	}
	return $translated_text;
}
add_filter( 'gettext', 'my_text_strings', 20, 3 );

//********************************* END Change Text ***************************************//



//********************************* Carousel Image ***************************************//

add_action( 'init', 'create_carousel_image_post_type' );
function create_carousel_image_post_type() {
  register_post_type( 'gt_carousel_image',
    array(
      'labels' => array(
        'name' => __( 'Carousel Images' ),
        'singular_name' => __( 'Carousel Image' )
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'carousel-images'),
      'show_in_menu' => true,
      'show_in_nav_menus'   => true,
      'supports' => ['title','editor','thumbnail']
    )
  );
}  

//********************************* END Carousel Image ***************************************//

add_action('gt_after_header', 'GTAddSearchBar');

function GTAddSearchBar()
{
	echo load_template(get_template_directory() . '/templates/search-bar.php');
}

//********************************* Custom Checkout Fields ***************************************//

add_action( 'woocommerce_checkout_get_value', 'GTCheckoutGetValue', 10, 2 );

function GTCheckoutGetValue($value, $input)
{
    if(!is_user_logged_in())
        return $value;
    switch($input){
        case 'payment_method':
            return get_user_meta(get_current_user_id(),'payment_method', true);
        case 'other_payment_method':
            return get_user_meta(get_current_user_id(),'other_payment_method', true);
        case 'preferred_carrier':
            return get_user_meta(get_current_user_id(),'preferred_carrier', true);
        case 'other_preferred_carrier':
            return get_user_meta(get_current_user_id(),'other_preferred_carrier', true);
        default:
            return $value;
    }
}

add_action( 'woocommerce_after_order_notes', 'OrderDetailsField' );

function OrderDetailsField( $checkout ) {

    echo '<div id="order_details_field">';
    woocommerce_form_field( 'payment_method', array(
        'type'          => 'select',
        'class'         => array('form-row-wide'),
        'label'         => __('Preferred Method of Payment <abbr class="required" title="required">*</abbr>'),
        'placeholder'   => __('Select a method of payment'),
        'options'		=> array(
        					'Credit Card' => 'Credit Card',
        					'Account' => 'Account',
        					'Other' => 'Other'
        					)
        ), WC()->checkout->get_value( 'payment_method' ));

    woocommerce_form_field( 'other_payment_method', array(
        'type'          => 'text',
        'class'         => array('form-row-wide hide-other'),
        'label'         => __('Other Preferred Method of Payment'),
        'placeholder'   => __('')
        ), WC()->checkout->get_value( 'other_payment_method' ));

    woocommerce_form_field( 'preferred_carrier', array(
        'type'          => 'select',
        'class'         => array('form-row-wide'),
        'label'         => __('Preferred Carrier <abbr class="required" title="required">*</abbr>'),
        'placeholder'   => __('Select a preferred carrier'),
        'options'		=> array(
        					'Loomis' => 'Loomis',
        					'Purolator' => 'Purolator',
        					'Greyhound' => 'Greyhound',
        					'CF' => 'CF',
        					'Fedex' => 'Fedex',
        					'Most Economical' => 'Most Economical',
        					'Other' => 'Other'
        					)
        ), WC()->checkout->get_value( 'preferred_carrier' ));

    woocommerce_form_field( 'other_preferred_carrier', array(
        'type'          => 'text',
        'class'         => array('form-row-wide hide-other'),
        'label'         => __('Other Preferred Carrier'),
        'placeholder'   => __('')
        ), WC()->checkout->get_value( 'other_preferred_carrier' ));

    $items = WC()->cart->get_cart();
    $hasLeaseItem = false;
    foreach($items as $item){
    	if(array_key_exists('is-lease', $item) && $item["is-lease"]){
    		$hasLeaseItem = true;
    		break;
    	}
    }

    if($hasLeaseItem){
	    woocommerce_form_field( 'lease_quote', array(
	        'type'          => 'select',
	        'label'         => __('We noticed that you are interested in leasing one or more products, would you also be interested in us providing lease rates for your entire quote?'),
	        'options'		=> array(
	        					'No' => 'No',
	        					'Yes' => 'Yes'
	        					)
	        ), $checkout->get_value( 'lease_quote' ));
	}
    echo '</div>';

}

add_action('woocommerce_checkout_process', 'OrderDetailsValidation');

function OrderDetailsValidation() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['payment_method'] )
        wc_add_notice( __( 'Please select a method of payment.' ), 'error' );
    if ( ! $_POST['preferred_carrier'] )
        wc_add_notice( __( 'Please select a preferred carrier.' ), 'error' );
}

add_action( 'woocommerce_checkout_update_order_meta', 'OrderDetailsUpdateOrderMeta' );

function OrderDetailsUpdateOrderMeta( $order_id ) {
    $order = new WC_Order($order_id);
    if ( ! empty( $_POST['payment_method'] ) ) {
        update_post_meta( $order_id, 'Method of Payment', sanitize_text_field( $_POST['payment_method'] ) );
        if($_POST['payment_method'] == "Other")
        	update_post_meta( $order_id, 'Other Method of Payment', sanitize_text_field( $_POST['other_payment_method'] ) );
        if ( $order->get_user_id() ) {
            update_user_meta( $order->get_user_id(), 'payment_method', $_POST['payment_method'] );
            if($_POST['payment_method'] == "Other")
                update_user_meta( $order->get_user_id(), 'other_payment_method', $_POST['other_payment_method'] );
        }
    }
    if ( ! empty( $_POST['preferred_carrier'] ) ) {
        update_post_meta( $order_id, 'Preferred Carrier', sanitize_text_field( $_POST['preferred_carrier'] ) );
        if($_POST['preferred_carrier'] == "Other")
        	update_post_meta( $order_id, 'Other Preferred Carrier', sanitize_text_field( $_POST['other_preferred_carrier'] ) );
        if ( $order->get_user_id() ) {
            update_user_meta( $order->get_user_id(), 'preferred_carrier', $_POST['preferred_carrier'] );
            if($_POST['preferred_carrier'] == "Other")
                update_user_meta( $order->get_user_id(), 'other_preferred_carrier', $_POST['other_preferred_carrier'] );
        }
    }
    if ( ! empty( $_POST['preferred_carrier'] ) ) {
        update_post_meta( $order_id, 'Lease Entire Quote', sanitize_text_field( $_POST['lease_quote'] ) );
    }

}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'OrderDetailsAdminDisplay', 10, 1 );

function OrderDetailsAdminDisplay($order){
    echo '<p><strong>'.__('Preferred Method of Payment').':</strong> ' . get_post_meta( $order->id, 'Method of Payment', true ) . '</p>';
    if(get_post_meta( $order->id, 'Method of Payment', true ) == 'Other')
    	echo '<p><strong>'.__('Other Preferred Method of Payment').':</strong> ' . get_post_meta( $order->id, 'Other Method of Payment', true ) . '</p>';
    echo '<p><strong>'.__('Preferred Carrier').':</strong> ' . get_post_meta( $order->id, 'Preferred Carrier', true ) . '</p>';
    if(get_post_meta( $order->id, 'Preferred Carrier', true ) == 'Other')
    	echo '<p><strong>'.__('Other Preferred Carrier').':</strong> ' . get_post_meta( $order->id, 'Other Preferred Carrier', true ) . '</p>';
    if(!empty(get_post_meta( $order->id, 'Lease Entire Quote', true )))
    	echo '<p><strong>'.__('Provide Lease Rates For the Entire Quote').':</strong> ' . get_post_meta( $order->id, 'Lease Entire Quote', true ) . '</p>';
}

//********************************* END Custom Checkout Fields ***************************************//

add_filter('woocommerce_add_cart_item_data', 'GTAddCartItemData');

function GTAddCartItemData($cart_item_data = NULL, $product_id = NULL, $variation_id = NULL)
{
	if(isset($_REQUEST['is-rental']) && $_REQUEST['is-rental']) {
		$cart_item_data['is-rental'] = $_REQUEST['is-rental'];
	}
	if(isset($_REQUEST['is-lease']) && $_REQUEST['is-lease']) {
		$cart_item_data['is-lease'] = $_REQUEST['is-lease'];
	}
    if(isset($_REQUEST['addons']) && $_REQUEST['addons']) {
        $cart_item_data['addons'] = $_REQUEST['addons'];
    }
	return $cart_item_data;
}

function GTGetCartItemsFromSession( $item, $values, $key ) {
    if ( array_key_exists( 'is-rental', $values ) )
        $item[ 'is-rental' ] = $values['is-rental'];
    if ( array_key_exists( 'is-lease', $values ) )
        $item[ 'is-lease' ] = $values['is-lease'];
    if ( array_key_exists( 'addons', $values ) )
        $item[ 'addons' ] = $values['addons'];
    return $item;
}
add_filter( 'woocommerce_get_cart_item_from_session', 'GTGetCartItemsFromSession', 1, 3 );

function GTGetItemData( $itemData, $cartItem ) {
    
   	if(isset($cartItem['is-rental']) && $cartItem['is-rental']){
   		$itemData[] = array(
			'key'   => 'Rental',
			'value' => 'Yes'
		);
   	}
   	if(isset($cartItem['is-lease']) && $cartItem['is-lease']){
   		$itemData[] = array(
			'key'   => 'Lease',
			'value' => 'Yes'
		);
   	}
    if(isset($cartItem['addons']) && $cartItem['addons']){
        $itemData[] = array(
            'key'   => 'Addons',
            'value' => $cartItem['addons']
        );
    }
   	return $itemData;
}
add_filter( 'woocommerce_get_item_data', 'GTGetItemData', 1, 3 );

function GTAddOrderItemMeta($item_id, $values, $cart_item_key) {
   if ( array_key_exists( 'is-rental', $values ) && $values['is-rental'])
   		wc_add_order_item_meta($item_id, 'Rental', 'Yes');
   if ( array_key_exists( 'is-lease', $values ) && $values['is-lease'])
   		wc_add_order_item_meta($item_id, 'Lease', 'Yes');
    if ( array_key_exists( 'addons', $values ) && $values['addons'])
        wc_add_order_item_meta($item_id, 'Addons', $values['addons']);
}
add_action('woocommerce_add_order_item_meta', 'GTAddOrderItemMeta', 10, 3);

add_action( 'woocommerce_before_add_to_cart_button', 'GTAddonsField', 30 );

function GTAddonsField()
{
    $addons = get_field('addons');
    if(!empty($addons))
        include(locate_template('templates/addons.php'));
}

add_filter('woocommerce_account_menu_items', 'GTRemoveAccountMenuItems');

function GTRemoveAccountMenuItems($items){
    unset($items['orders']);
    unset($items['downloads']);
    return $items;
}

//********************************* Select All For Newsletter Subscription Form ***************************************//

add_filter( 'gform_field_input', 'GTSelectAll', 10, 5 );
function GTSelectAll( $input, $field, $value, $lead_id, $form_id ) {
    if ( $field->cssClass == 'select_all' ) {
        $input = '<input type="checkbox" name="select-all" /><label for="select-all" style="margin-left: 0.8rem">Select All</label>';      
    }
    return $input;
}

//********************************* END Select All For Newsletter Subscription Form ***************************************//