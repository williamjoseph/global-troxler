<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $post;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<div class="add-to-cart">
		<?php if ( ! $product->is_sold_individually() ) : ?>
			<p>Qty:</p>
			<button class="button secondary remove-quantity"><i class="fa fa-minus"></i></button>
			<?php woocommerce_quantity_input( array( 'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 ) ); ?>
			<button class="button secondary add-quantity"><i class="fa fa-plus"></i></button>
		<?php endif; ?>
		
		<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->id ); ?>" />
		<input type="hidden" name="product_id" value="<?php echo absint( $product->id ); ?>" />
		<input type="hidden" name="variation_id" class="variation_id" value="0" />
		<div class="clear"></div>
	</div>
	<div class="add-to-cart-buttons">
		<div class="cart-button-container">
			<button type="submit" class="single_add_to_cart_button button alt"><?= __('Add to Cart', 'woocommerce');?><i class="fa fa-cart-plus"></i></button>
		</div>
	<?php 
		$isRental = get_field('is_rental');
		$isLeaseable = get_field('is_leaseable');
		if($isRental || $isLeaseable){ 
			if($isRental){ ?>
				<button class="button rent-product property-button">Rent this <br/>Product <i class="fa fa-chevron-right"></i></button>
			<?php } if($isLeaseable) { ?>
				<button class="button lease-product property-button">Lease this <br/>Product <i class="fa fa-chevron-right"></i></button>
	<?php }} else { ?>
		<div class="contact">
			<p>Contact ></p>
			<p><span class="yellow">+</span>1.866.300.9012</p>
		</div>
	<?php } ?>
	</div>
	<div class="clear"></div>
</div>
