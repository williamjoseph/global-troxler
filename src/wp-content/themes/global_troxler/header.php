<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
		<link rel="shortcut icon" href="<?=get_stylesheet_directory_uri();?>/favicon.ico" />
		<?php wp_head(); ?>
		<?php if(get_site_url() == 'http://www.globaltroxler.ca') { ?>
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-89860085-1', 'auto');
			  ga('send', 'pageview');

			</script>
		<?php } ?>
	</head>
	<body <?php body_class((get_bloginfo('name') == 'Training') ? 'training' : ''); ?>>
		<div class="mobile-nav small-12 show-for-small-only" id="slideout">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu','container_class' => 'mobile-nav' ) ); ?>
		</div>
		<div id="wrapper" class="hfeed">
			<header id="header" role="banner">
				<nav id="top-menu" role="navigation" class="blue-bar hide-for-small-only">
					<?php wp_nav_menu( array( 'theme_location' => 'top-menu' , 'container_class' => 'row align-middle align-right', 'menu_class' => 'menu') ); ?>
				</nav>
				<nav id="menu" role="navigation" class="row align-middle">
					<?php $logo = (get_bloginfo('name') == 'Training') ? 'logo-border' : 'logo'; ?>

					<div class="columns medium-4 small-9" id="image-container"><a href="<?=(get_bloginfo('name') == 'Training') ? get_site_url().'/courses-overview' : get_site_url();?>"><img src="<?= get_stylesheet_directory_uri(); ?>/images/<?=$logo;?>.png" alt="Global Troxler" /></a></div>
					<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'container_class' => 'columns medium-8 hide-for-small-only full-menu', 'menu_class' => 'menu dropdown' ) ); ?>
					<!-- <div class="columns medium-8 hide-for-small-only full-menu"> -->
					<div class="mobile-toggle columns small-3 show-for-small-only">
						<span class="fa fa-bars"></span>
					</div>
					
					<div id="category-dropdown" class="sub-menu">
						<ul class="menu">
						<?php 

							$categories = get_terms('product_cat', array('hide_empty' => false, 'orderby' => 'order'));
							foreach($categories as $category){ 
								if($category->name == 'Uncategorized')
									continue;
								if($category->parent == 0) { 
									echo "<li class='category-group'><ul>";
									echo "<li class=\"unclickable\">{$category->name}</li>";
									foreach($categories as $subCategory){
										if($subCategory->parent == $category->term_id)
											echo "<li><a href=\"".get_site_url()."/product-category/{$subCategory->slug}/\">$subCategory->name</a></li>";
									}
									echo '</ul></li>';
								}

							} 
						?>
						</ul>
					</div>	
				</nav>
				<?php do_action('gt_after_header'); ?>
			</header>
			<div id="container">