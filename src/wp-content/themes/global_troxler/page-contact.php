<?php get_header(); ?>
<section id="content" role="main" class="row contact">
	<?php 
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post(); 
	?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('medium-12 columns'); ?>>
		<header class="header">
			<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
		</header>
		<section class="entry-content row">
			<div class="columns medium-8 small-12 main-content">
				<?php the_content(); ?>
			</div>
			<div class="columns medium-4 small-12 contact-info">
				<p>Phone: <a href="tel:+1-866-300-9012">+1-866-300-9012</a></p>
				<p>Email: <a href="mailto:contact@measur.ca">contact@measur.ca</a></p>
				<p style="margin: 0;">Calgary Head Office:</p>
				<address>2 - 1540 Hastings Cres. S.E. <br/>Calgary, AB.<br/> T2G 4E1</address>
				<div id="contact-map" class="contact-map"></div><br/>
				<p style="margin: 0;">Burnaby Office:</p>
				<address>7 - 5850 Byrne Road<br/> Burnaby, BC.<br/> V5J 3J3</address>
				<div id="contact-map-burnaby" class="contact-map"></div>
			</div>
		</section>
	</article>
	<?php } 
	} ?>
</section>

<script>
	// Contact Page Map
	var map;
	var burnabyMap;
	function initMap() {
		 
		var myLatLng = {lat: 51.015026, lng: -114.031729};
		
		var myStyles = [
			{
					featureType: "poi",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
			}
		]
		
		map = new google.maps.Map(document.getElementById('contact-map'), {
			center: myLatLng,
			zoom: 17,
			styles: myStyles
			// scrollwheel: false,
			// draggable: false,
			// disableDefaultUI: true
		});
		
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: "Calgary Head Office"
		});

		var myLatLng = {lat: 49.203480, lng: -122.978870};

		burnabyMap = new google.maps.Map(document.getElementById('contact-map-burnaby'), {
			center: myLatLng,
			zoom: 17,
			styles: myStyles
			// scrollwheel: false,
			// draggable: false,
			// disableDefaultUI: true
		});
		
		var burnabyMarker = new google.maps.Marker({
			position: myLatLng,
			map: burnabyMap,
			title: "Burnaby Office"
		});
		
	}
	

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD94bx0LulTIKyYjPR1T2wumqWnUNMulxk&callback=initMap"
    async defer></script>
<?php get_footer(); ?>