<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class('columns small-12'); ?>>
	<?=the_title( '<h1 itemprop="name" class="product_title entry-title">', '</h1>' );?>
	<div class="row">

		<div class="columns medium-3 small-12">
			<?php
				/**
				 * woocommerce_before_single_product_summary hook.
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>
			
			<?php 
				$options = get_option( 'gt_options' );
				$showFeatured = $options['single_display_featured'];
				if($showFeatured){
			?>
			<div class="featured-product">
				<h1 class="entry-title">Featured Product</h1>
				<div>
				<?php $args = array(  
					    'post_type' => 'product',  
					    'meta_key' => '_featured',  
					    'meta_value' => 'yes',  
					    'posts_per_page' => 1,
					    'orderby' => 'rand',
					    'post__not_in' => [get_the_ID()]  
					);  
					  
					$featured_query = new WP_Query( $args );  
					if ($featured_query->have_posts()) {   
					  
					    while ( $featured_query->have_posts() ) { 
					    	$featured_query->the_post(); ?>
					      
					        <div <?php post_class('product-item'); ?>>
					        	<?php wc_get_template_part( 'content', 'product' ); ?>
					        	<div class="description"><?php the_excerpt(); ?></div>
					        	<a class="button" href="<?=get_the_permalink();?>">View Product</a>
					        </div>  
					          
					<?php }  
					      
					}  
					  
					wp_reset_query();
				?>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="columns medium-9 small-12">
			<div class="summary entry-summary">

				<?php
					/**
					 * woocommerce_single_product_summary hook.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					do_action( 'woocommerce_single_product_summary' );
				?>

			</div><!-- .summary -->
		
		<?php
			/**
			 * woocommerce_after_single_product_summary hook.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
		</div>
		<meta itemprop="url" content="<?php the_permalink(); ?>" />
	</div>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
