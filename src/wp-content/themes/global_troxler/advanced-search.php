<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php // get_search_form(); ?>

<form method="get" action="<?=get_site_url();?>">
	<label for="title">Title</label>
	<input type="text" name="title" value=""/>

	<label for="description">Description</label>
	<input type="text" name="description" value=""/>

	<label for="sku">Product Number</label>
	<input type="text" name="sku" value=""/>

	<input type="hidden" value="product" name="post_type" />

	<button type="submit" class="button">Search</button>
</form>