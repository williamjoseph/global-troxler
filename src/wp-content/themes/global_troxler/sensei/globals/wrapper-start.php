<?php
/**
 * Content wrappers Start
 *
 * All support theme wrappers can be found in includes/theme-integrations
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
?>

<div id="content" class="page col-full row" role="main">
    <div id="main" class="col-left columns medium-12">