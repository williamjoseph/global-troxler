<div id="search-bar">
	<div class="row  align-middle">
		<div class="search-field columns medium-8 small-12">
			<?php get_product_search_form(); ?>
		</div>
		<div class="medium-4 small-12 columns" id="search-buttons">
			<ul class="menu">
				<li><a href="<?=get_site_url();?>/advanced-search">Advanced Search</a></li>
				<li><a href="<?=get_site_url();?>/cart" <?=(WC()->cart->get_cart_contents_count() > 0) ? 'class="selected"' : '';?>><?= __('Cart', 'woocommerce');?> <?= (WC()->cart->get_cart_contents_count() > 0) ? "(".WC()->cart->get_cart_contents_count().")" : '';?> <i class="fa fa-shopping-cart"></i></a></li>
			</ul>
		</div>
	</div>
</div>