<?php

/*
Plugin Name: Duplicate Category
Description: Ability to copy categories and products to another category
Version: 1.0
Author: Kelly Anderson for William Joseph
Author URI: http://www.williamjoseph.com
*/

add_action( 'admin_action_duplicate', 'WJDCDuplicate');
function WJDCDuplicate()
{
    set_time_limit(0);
    $fromCategory = (int) $_POST['from-category'];
    $toCategory = (int) $_POST['to-category'];
    if($_POST['include-products']){
        // We want to call this with $includeParents true here so that all of the products will have the correct parents
        WJDCSetProductCategory($fromCategory, $toCategory, true);
    }
    WJDCDuplicateCategories(WJDCGetCategories($fromCategory), $toCategory, $_POST['include-products']);
    
    header("Location: " . $_SERVER['HTTP_REFERER'] . '&success=1');
    exit;
}

function WJDCGetCategories($parentCategoryID)
{   
    $args = [
        'taxonomy' => 'product_cat',
        'hierarchical' => 1,
        'hide_empty' => 0,
        'parent' => $parentCategoryID
    ];
    $categories = get_categories($args);
    return $categories;
}

function WJDCDuplicateCategories($categories, $copyToCategoryID, $includeProducts)
{
    foreach($categories as $subCat){
        $newCategory = term_exists($subCat->name, 'product_cat', $copyToCategoryID);
        if(!$newCategory){
            $newCategory = wp_insert_term($subCat->name, 'product_cat', ['description'=>$subCat->description, 'parent'=>$copyToCategoryID, 'slug'=>$subCat->slug]);
            
            $subCatTermOptions = get_option( "taxonomy_".$subCat->term_id);
            $newCatTermOptions = get_option( "taxonomy_".$newCategory['term_id']);

            if(isset($subCatTermOptions['custom_term_grouping']))
                $newCatTermOptions['custom_term_grouping'] = $subCatTermOptions['custom_term_grouping'];
            if(isset($subCatTermOptions['custom_term_collapsed']))
                $newCatTermOptions['custom_term_collapsed'] = $subCatTermOptions['custom_term_collapsed'];

            update_option("taxonomy_".$newCategory['term_id'], $newCatTermOptions);
        }
        

        // Get Products!
        if($includeProducts){
            $newCategoryID = (int) $newCategory['term_id'];
            WJDCSetProductCategory($subCat->term_id, $newCategoryID);
        }
        WJDCDuplicateCategories(WJDCGetCategories($subCat->term_id), $newCategoryID, $includeProducts);
    }
}

function WJDCSetProductCategory($parentCategoryID, $newCategoryID, $includeParents = NULL) {
    $addCategories = [$newCategoryID];
    if($includeParents){
        $currentCategory = get_term($newCategoryID);
        while(!empty($currentCategory->parent)){
            $addCategories[] = $currentCategory->parent;
            $currentCategory = get_term($currentCategory->parent);
        }
    }
    $args = [
        'post_type' => 'product',
        'posts_per_page' => -1,
        'tax_query' => [
            [
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $parentCategoryID,
                'operator' => 'IN'
            ]
        ]
    ];

    $products = new WP_Query($args);

    while($products->have_posts()) {
        $products->the_post(); 
        global $product;

        $result = wp_set_object_terms($product->id, $addCategories, 'product_cat', true);
    }
}

add_action('admin_menu', 'WJDCCreateMenu');

function WJDCCreateMenu() {

    //create new top-level menu
    add_options_page('Duplicate Subcategories', 'Duplicate Subcategories', 'manage_options', 'wjdc-settings-page' , 'WJDCPage');

    //call register settings function
    // add_action( 'admin_init', 'register_my_cool_plugin_settings' );
}

function WJDCPage() {
    include(plugin_dir_path(__FILE__).'admin-page.php');
}

