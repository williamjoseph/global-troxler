<div class="wrap">
    <h1>Duplicate Subcategories</h1>
    <?php if(isset($_GET['success']) && !empty($_GET['success'])){ ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Duplication Complete!'); ?></p>
        </div>
        <?php } ?>
    <form method="post" action="<?php echo admin_url( 'admin.php' ); ?>" id="duplicate-form">
        <table class="form-table">
            <tbody>
                <tr>
                    <td>
                        <label for="from-category">From</label>
                    </td>
                    <td>
                        <?php 
                            $args = [
                                'taxonomy' => 'product_cat',
                                'hierarchical' => 1,
                                'hide_empty' => 0,
                                'name' => 'from-category'
                            ];
                            wp_dropdown_categories($args); 
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="to-category">To</label>
                    </td>
                    <td>
                        <?php 
                            $args = [
                                'taxonomy' => 'product_cat',
                                'hierarchical' => 1,
                                'hide_empty' => 0,
                                'name' => 'to-category'
                            ];
                            wp_dropdown_categories($args); 
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="include-products">Include Products</label>
                    </td>
                    <td>
                        <input type="checkbox" name="include-products" id="include-products">
                    </td>
                </tr>
            </tbody>
        </table>
        
        <p class="submit">
            <input type="hidden" name="action" value="duplicate" />
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Duplicate!" />
            <img src="<?=get_site_url();?>/wp-admin/images/wpspin_light.gif" style="padding: 0 1em; display: none" id="loader" />
        </p>

    </form>
</div>

<script type="text/javascript">
    var form = document.getElementById('duplicate-form');
    var submitButton = document.getElementById('submit');
    var loader = document.getElementById('loader');
    form.onsubmit = function(){
        submitButton.disabled = true;
        loader.style.display = 'inline-block';
    }

</script>