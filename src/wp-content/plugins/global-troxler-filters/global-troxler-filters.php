<?php 
/*
Plugin Name: Global Troxler Filters
Description: These are the filters for the sidebar.
Version: 1.0
Author: Kelly Anderson for William Joseph
Author URI: http://www.williamjoseph.com
*/

class GlobalTroxlerFiltersWidget extends WP_Widget {

function __construct() {
	parent::__construct(
	// Base ID of your widget
	'global_troxler_filters', 

	// Widget name will appear in UI
	__('Global Troxler Filters', 'global_troxler'), 

	// Widget description
	array( 'description' => __( 'These are the filters for the sidebar', 'global_troxler' ), ) 
	);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) )
	// echo $args['before_title'] . $title . $args['after_title'];

	echo '<ul class="no-bullet">';
	// This is where you run the code and display the output
	$currentCategoryID = $this->get_current_term_id();
	$categories = get_terms( 'product_cat', ['hide_empty'=>true, 'child_of' => $currentCategoryID] );
	$this->layered_categories( $categories, 'product_cat', 'and');

	$attributes = wc_get_attribute_taxonomies();
	if(!empty($attributes)){

		foreach($attributes as $attribute){
			if($attribute->attribute_name == 'rental')
				continue;
			$taxonomy = 'pa_'.$attribute->attribute_name;
			$terms = get_terms($taxonomy, ['hide_empty'=>true]);
			$list = $this->layered_nav_list($terms, $taxonomy, 'and');
			if(empty($list))
				continue;
			echo '<li><span class="parent-category">'.$attribute->attribute_label.'</span>';;
			echo '<ul class="no-bullet">';
			foreach($list as $item)
				echo $item;
			echo '</ul>';
			echo '<li>';
		}
	}
	echo '</ul>';
	echo $args['after_widget'];
}

/**
 * Return the currently viewed taxonomy name.
 * @return string
 */
protected function get_current_taxonomy() {
	return is_tax() ? get_queried_object()->taxonomy : '';
}

/**
 * Return the currently viewed term ID.
 * @return int
 */
protected function get_current_term_id() {
	return absint( is_tax() ? get_queried_object()->term_id : 0 );
}

/**
 * Return the currently viewed term slug.
 * @return int
 */
protected function get_current_term_slug() {
	return absint( is_tax() ? get_queried_object()->slug : 0 );
}

/**
 * Get current page URL for layered nav items.
 * @return string
 */
protected function get_page_base_url( $taxonomy, $newCategory = NULL ) {
	if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
		$link = home_url();
	} elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id( 'shop' ) ) ) {
		$link = get_post_type_archive_link( 'product' );
	} elseif ( is_product_category() ) {
		$link = get_term_link( get_query_var( 'product_cat' ), 'product_cat' );
	} elseif ( is_product_tag() ) {
		$link = get_term_link( get_query_var( 'product_tag' ), 'product_tag' );
	} else {
		$queried_object = get_queried_object();
		$link = get_term_link( $queried_object->slug, $queried_object->taxonomy );
	}
	if(!empty(get_query_var( 'product_cat' )))
		$link = get_term_link( get_query_var( 'product_cat' ), 'product_cat' );
	if(!empty($newCategory))
		$link = get_site_url()."/product-category/".$newCategory->slug;

	// Min/Max
	if ( isset( $_GET['min_price'] ) ) {
		$link = add_query_arg( 'min_price', wc_clean( $_GET['min_price'] ), $link );
	}

	if ( isset( $_GET['max_price'] ) ) {
		$link = add_query_arg( 'max_price', wc_clean( $_GET['max_price'] ), $link );
	}

	// Orderby
	if ( isset( $_GET['orderby'] ) ) {
		$link = add_query_arg( 'orderby', wc_clean( $_GET['orderby'] ), $link );
	}

	/**
	 * Search Arg.
	 * To support quote characters, first they are decoded from &quot; entities, then URL encoded.
	 */
	if ( get_search_query() ) {
		$link = add_query_arg( 's', rawurlencode( htmlspecialchars_decode( get_search_query() ) ), $link );
	}

	// Post Type Arg
	if ( isset( $_GET['post_type'] ) ) {
		$link = add_query_arg( 'post_type', wc_clean( $_GET['post_type'] ), $link );
	}

	// Min Rating Arg
	if ( isset( $_GET['min_rating'] ) ) {
		$link = add_query_arg( 'min_rating', wc_clean( $_GET['min_rating'] ), $link );
	}

	// Post Type Arg
	if ( isset( $_GET['title'] ) ) {
		$link = add_query_arg( 'title', wc_clean( $_GET['title'] ), $link );
	}

	// Post Type Arg
	if ( isset( $_GET['description'] ) ) {
		$link = add_query_arg( 'description', wc_clean( $_GET['description'] ), $link );
	}

	// Post Type Arg
	if ( isset( $_GET['short_description'] ) ) {
		$link = add_query_arg( 'short_description', wc_clean( $_GET['short_description'] ), $link );
	}

	// Post Type Arg
	if ( isset( $_GET['sku'] ) ) {
		$link = add_query_arg( 'sku', wc_clean( $_GET['sku'] ), $link );
	}

	// Post Type Arg
	if ( isset( $_GET['rental'] ) ) {
		$link = add_query_arg( 'rental', wc_clean( $_GET['rental'] ), $link );
	}

	// All current filters
	if ( $_chosen_attributes = WC_Query::get_layered_nav_chosen_attributes() ) {
		foreach ( $_chosen_attributes as $name => $data ) {
			if ( $name === $taxonomy ) {
				continue;
			}
			$filter_name = sanitize_title( str_replace( 'pa_', '', $name ) );
			if ( ! empty( $data['terms'] ) ) {
				$link = add_query_arg( 'filter_' . $filter_name, implode( ',', $data['terms'] ), $link );
			}
			if ( 'or' == $data['query_type'] ) {
				$link = add_query_arg( 'query_type_' . $filter_name, 'or', $link );
			}
		}
	}

	return $link;
}

/**
 * Count products within certain terms, taking the main WP query into consideration.
 * @param  array $term_ids
 * @param  string $taxonomy
 * @param  string $query_type
 * @return array
 */
protected function get_filtered_term_product_counts( $term_ids, $taxonomy, $query_type ) {
	global $wpdb;

	$tax_query  = WC_Query::get_main_tax_query();
	$meta_query = WC_Query::get_main_meta_query();

	if ( 'or' === $query_type ) {
		foreach ( $tax_query as $key => $query ) {
			if ( $taxonomy === $query['taxonomy'] ) {
				unset( $tax_query[ $key ] );
			}
		}
	}

	$meta_query      = new WP_Meta_Query( $meta_query );
	$tax_query       = new WP_Tax_Query( $tax_query );
	$meta_query_sql  = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
	$tax_query_sql   = $tax_query->get_sql( $wpdb->posts, 'ID' );

	// Generate query
	$query           = array();
	$query['select'] = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) as term_count, terms.term_id as term_count_id";
	$query['from']   = "FROM {$wpdb->posts}";
	$query['join']   = "
		INNER JOIN {$wpdb->term_relationships} AS term_relationships ON {$wpdb->posts}.ID = term_relationships.object_id
		INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy USING( term_taxonomy_id )
		INNER JOIN {$wpdb->terms} AS terms USING( term_id )
		" . $tax_query_sql['join'] . $meta_query_sql['join'];

	$query['where']   = "
		WHERE {$wpdb->posts}.post_type IN ( 'product' )
		AND {$wpdb->posts}.post_status = 'publish'
		" . $tax_query_sql['where'] . $meta_query_sql['where'] . "
		AND terms.term_id IN (" . implode( ',', array_map( 'absint', $term_ids ) ) . ")
	";

	if ( $search = WC_Query::get_main_search_query_sql() ) {
		$query['where'] .= ' AND ' . $search;
	}

	$query['group_by'] = "GROUP BY terms.term_id";
	$query             = apply_filters( 'woocommerce_get_filtered_term_product_counts_query', $query );
	$query             = implode( ' ', $query );
	$results           = $wpdb->get_results( $query );

	return wp_list_pluck( $results, 'term_count', 'term_count_id' );
}

/**
 * Show list based layered nav.
 * @param  array $terms
 * @param  string $taxonomy
 * @param  string $query_type
 * @return bool Will nav display?
 */
protected function layered_categories( $terms, $taxonomy, $query_type ) {
	// List display

	$term_counts        = $this->get_filtered_term_product_counts( wp_list_pluck( $terms, 'term_id' ), $taxonomy, $query_type );
	$_chosen_attributes = WC_Query::get_layered_nav_chosen_attributes();
	$found              = false;

	$termsWithItems = [];
	foreach ( $terms as $term ) {
		$current_values    = isset( $_chosen_attributes[ $taxonomy ]['terms'] ) ? $_chosen_attributes[ $taxonomy ]['terms'] : array();
		$option_is_set     = in_array( $term->slug, $current_values );
		$count             = isset( $term_counts[ $term->term_id ] ) ? $term_counts[ $term->term_id ] : 0;

		// skip the term for the current archive
		if ( $this->get_current_term_id() === $term->term_id ) {
			continue;
		}

		// Only show options with count > 0
		if ( 0 < $count ) {
			// $found = true;
			$termsWithItems[] = $term;
		} elseif ( 'and' === $query_type && 0 === $count && ! $option_is_set ) {
			continue;
		}

		
	}

	$otherCategories = [];
	foreach($termsWithItems as $term) {
		$currentCategoryID = $this->get_current_term_id();
		if($term->parent != $currentCategoryID)
			continue;
		
		$term_grouping = get_option( "taxonomy_$term->term_id" );
        $grouping_value = esc_attr( $term_grouping['custom_term_grouping'] );
        $collapsed_value = esc_attr( $term_grouping['custom_term_collapsed'] );
        if($grouping_value != "Yes"){
        	$otherCategories[] = $term;
        	continue;
        }
        $class = '';
        if($collapsed_value == "Yes")
        	$class = 'class="closed"';
		echo '<li '.$class.'><span class="parent-category">'.$term->name.'</span>';
		echo '<ul class="no-bullet">';
		foreach($termsWithItems as $subCat){
			if($subCat->parent != $term->term_id)
				continue;
			$this->BuildCategoryItem($subCat, $term_counts);
		}
		echo '</ul></li>';
	}

	if(!empty($otherCategories)){
		echo '<li class="closed"><span class="parent-category">Other</span>';
		echo '<ul  class="no-bullet">';
		foreach($otherCategories as $subCat){
			$this->BuildCategoryItem($subCat, $term_counts);
		}
		echo '</ul></li>';
	}


	return $found;
}

protected function CreateListItem($item, $link, $count, $isSet = FALSE)
{
	$listItem = "";

	$listItem .= '<li class="wc-layered-nav-term ' . ( $isSet ? 'chosen' : '' ) . '">';	

	$listItem .= ($count > 0 || $isSet) ? '<a href="' . esc_url( apply_filters( 'woocommerce_layered_nav_link', $link ) ) . '">' : '<span>';

	if($item->taxonomy != 'product_cat')
		$listItem .= ($isSet) ? '<span class="fa fa-check-square-o"></span>' : '<span class="fa fa-square-o"></span>';

	$listItem .= esc_html( $item->name );

	$listItem .= ($count > 0 || $isSet) ? '</a> ' : '</span> ';

	// $listItem .= apply_filters( 'woocommerce_layered_nav_count', '<span class="count">(' . absint( $count ) . ')</span>', $count, $term );

	$listItem .= '</li>';

	return $listItem;
}

protected function BuildCategoryItem($category, $term_counts) 
{
	$current_values    = isset( $_chosen_attributes[ $taxonomy ]['terms'] ) ? $_chosen_attributes[ $taxonomy ]['terms'] : array();
	$option_is_set     = in_array( $category->slug, $current_values );
	$count             = isset( $term_counts[ $category->term_id ] ) ? $term_counts[ $category->term_id ] : 0;

	$filter_name    = 'filter_' . sanitize_title( str_replace( 'pa_', '', $taxonomy ) );
	$current_filter = isset( $_GET[ $filter_name ] ) ? explode( ',', wc_clean( $_GET[ $filter_name ] ) ) : array();
	$current_filter = array_map( 'sanitize_title', $current_filter );

	if ( ! in_array( $category->slug, $current_filter ) ) {
		$current_filter[] = $category->slug;
	}

	$link = $this->get_page_base_url( $taxonomy, $category );

	// Add current filters to URL.
	foreach ( $current_filter as $key => $value ) {
		// Exclude query arg for current term archive term
		if ( $value === $this->get_current_term_slug() ) {
			unset( $current_filter[ $key ] );
		}

		// Exclude self so filter can be unset on click.
		if ( $option_is_set && $value === $category->slug ) {
			unset( $current_filter[ $key ] );
		}
	}

	// if ( ! empty( $current_filter ) ) {
	// 	$link = add_query_arg( $filter_name, implode( ',', $current_filter ), $link );

	// 	// Add Query type Arg to URL
	// 	if ( $query_type === 'or' && ! ( 1 === sizeof( $current_filter ) && $option_is_set ) ) {
	// 		$link = add_query_arg( 'query_type_' . sanitize_title( str_replace( 'pa_', '', $taxonomy ) ), 'or', $link );
	// 	}
	// }

	echo $this->CreateListItem($category, $link, $count, $option_is_set);
}

/**
 * Show list based layered nav.
 * @param  array $terms
 * @param  string $taxonomy
 * @param  string $query_type
 * @return bool Will nav display?
 */
protected function layered_nav_list( $terms, $taxonomy, $query_type) {
	// List display
	$term_counts        = $this->get_filtered_term_product_counts( wp_list_pluck( $terms, 'term_id' ), $taxonomy, $query_type );
	$_chosen_attributes = WC_Query::get_layered_nav_chosen_attributes();
	$listItems          = [];
	$found 				= false;

	foreach ( $terms as $term ) {
		$current_values    = isset( $_chosen_attributes[ $taxonomy ]['terms'] ) ? $_chosen_attributes[ $taxonomy ]['terms'] : array();
		$option_is_set     = in_array( $term->slug, $current_values );
		$count             = isset( $term_counts[ $term->term_id ] ) ? $term_counts[ $term->term_id ] : 0;

		// skip the term for the current archive
		if ( $this->get_current_term_id() === $term->term_id ) {
			continue;
		}

		// Only show options with count > 0
		if ( 0 < $count ) {
			$found = true;
		} elseif ( 'and' === $query_type && 0 === $count && ! $option_is_set ) {
			continue;
		}

		$filter_name    = 'filter_' . sanitize_title( str_replace( 'pa_', '', $taxonomy ) );
		$current_filter = isset( $_GET[ $filter_name ] ) ? explode( ',', wc_clean( $_GET[ $filter_name ] ) ) : array();
		$current_filter = array_map( 'sanitize_title', $current_filter );

		if ( ! in_array( $term->slug, $current_filter ) ) {
			$current_filter[] = $term->slug;
		}

		$link = $this->get_page_base_url( $taxonomy );

		// Add current filters to URL.
		foreach ( $current_filter as $key => $value ) {
			// Exclude query arg for current term archive term
			if ( $value === $this->get_current_term_slug() ) {
				unset( $current_filter[ $key ] );
			}

			// Exclude self so filter can be unset on click.
			if ( $option_is_set && $value === $term->slug ) {
				unset( $current_filter[ $key ] );
			}
		}

		if ( ! empty( $current_filter ) ) {
			$link = add_query_arg( $filter_name, implode( ',', $current_filter ), $link );

			// Add Query type Arg to URL
			if ( $query_type === 'or' && ! ( 1 === sizeof( $current_filter ) && $option_is_set ) ) {
				$link = add_query_arg( 'query_type_' . sanitize_title( str_replace( 'pa_', '', $taxonomy ) ), 'or', $link );
			}
		}

		$listItems[] = $this->CreateListItem($term, $link, $count, $option_is_set);
	}

	return $listItems;
}
		
// Widget Backend 
public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
		}
		else {
		$title = __( 'New title', 'global_troxler' );
	}
	// Widget admin form
	?>
	<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<?php 
}
	
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}

// Register and load the widget
function global_troxler_filters_load_widget() {
	register_widget( 'GlobalTroxlerFiltersWidget' );
}
add_action( 'widgets_init', 'global_troxler_filters_load_widget' );