<?php

/*
Plugin Name: Global Troxler Training
Description: Changes for the training part of the site
Version: 1.0
Author: Kelly Anderson for William Joseph
Author URI: http://www.williamjoseph.com
*/

add_action('gt_after_header', 'GTAddTrainingSearchBar');

function GTAddTrainingSearchBar()
{
	echo load_template(get_template_directory() . '/templates/training-search-bar.php');
}


add_filter( 'wp_nav_menu_items', 'GTAddLoginLogout', 10, 2 );
function GTAddLoginLogout( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'main-menu') {
        $items .= '<li><a href="'. wp_logout_url(get_permalink()) .'">Log Out</a></li>';
    }
    elseif (!is_user_logged_in() && $args->theme_location == 'main-menu') {
        $items .= '<li><a href="'. get_site_url() . '/my-courses">Log In</a></li>';
    }
    return $items;
}
