<div id="search-bar">
	<div class="row  align-middle">
		<div class="search-field columns medium-8 small-12">
			<form role="search" method="get" action="<?=get_site_url();?>/courses-overview">
				<input type="search" class="search-field" placeholder="Search Courses" name="s" />
				<button type="submit" class="fa fa-search"></button>
			</form>
		</div>
		<div class="medium-4 small-12 columns" id="search-buttons">
			<ul class="menu">
				<li><a href="<?=get_site_url();?>/cart" <?=(WC()->cart->get_cart_contents_count() > 0) ? 'class="selected"' : '';?>><?= __('Cart', 'woocommerce');?> <?= (WC()->cart->get_cart_contents_count() > 0) ? "(".WC()->cart->get_cart_contents_count().")" : '';?> <i class="fa fa-shopping-cart"></i></a></li>
			</ul>
		</div>
	</div>
</div>