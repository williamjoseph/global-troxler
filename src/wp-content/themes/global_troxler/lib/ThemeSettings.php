<?php
class ThemeSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'AddThemePage' ) );
        add_action( 'admin_init', array( $this, 'PageInit' ) );
    }

    /**
     * Add options page
     */
    public function AddThemePage()
    {
        // This page will be under "Settings"
        add_options_page(
            'Global Troxler Settings', 
            'Global Troxler Settings', 
            'manage_options', 
            'gt-settings-admin', 
            array( $this, 'CreateAdminPage' )
        );
    }

    /**
     * Options page callback
     */
    public function CreateAdminPage()
    {
        // Set class property
        $this->options = get_option( 'gt_options' );
        ?>
        <div class="wrap">
            <h1>Global Troxler Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'gt_options' );
                do_settings_sections( 'gt-settings-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function PageInit()
    {        
        register_setting(
            'gt_options', // Option group
            'gt_options', // Option name
            array( $this, 'Sanitize' ) // Sanitize
        );

        add_settings_section(
            'gt_single_product', // ID
            'Single Product page', // Title
            array( $this, 'PrintSectionInfo' ), // Callback
            'gt-settings-admin' // Page
        );  

        add_settings_field(
            'single_display_featured', // ID
            'Display Featured Product', // Title 
            array( $this, 'DisplayFeaturedCallback' ), // Callback
            'gt-settings-admin', // Page
            'gt_single_product' // Section           
        );         
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function Sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['single_display_featured'] ) )
            $new_input['single_display_featured'] = $input['single_display_featured'];

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function PrintSectionInfo()
    {
        print 'Enter your settings below:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function DisplayFeaturedCallback()
    {
        printf(
            '<input type="checkbox" id="single_display_featured" name="gt_options[single_display_featured]" %s />',
            ($this->options['single_display_featured']) ? 'checked="checked"' : ''
        );
    }
}

if( is_admin() )
    $themeSettingsPage = new ThemeSettings();