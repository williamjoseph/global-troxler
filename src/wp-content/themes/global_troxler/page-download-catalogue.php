<?php get_header(); ?>
<section id="content" role="main" class="row">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('medium-12 columns'); ?>>
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<section class="entry-content">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
<?php the_content(); ?>
<?php
	$documents = get_field('documents');
	if(!empty($documents)){ 
?>
	<ul class="no-bullet files-list">
		<?php foreach($documents as $document){ ?>
			<li><a href='<?=$document['file'];?>' class='product-manual-link' target='_blank'><i class='fa fa-file-pdf-o'></i> <?=$document['title'];?></a></li>
		<?php } ?>
	</ul>
<?php } ?>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>